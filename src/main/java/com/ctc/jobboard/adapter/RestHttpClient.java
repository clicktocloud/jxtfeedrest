package com.ctc.jobboard.adapter;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class RestHttpClient {
	
	
	
	private static int defaultRetryTimes = 4;
	
    private static Logger logger = LoggerFactory.getLogger("com.ctc.jobboard");
    
    private CloseableHttpClient httpclient;
    
    private PoolingHttpClientConnectionManager cm;
    
    public RestHttpClient(){
    	
    	httpclient  = HttpClients.createDefault();
    }
    
    public RestHttpClient(int minPoolSize, int maxPoolSize, int connectionTimeout, int socketTimeout){
    	super();
    	RequestConfig requestConfig = RequestConfig.custom()
    			.setConnectTimeout(connectionTimeout * 1000)
    			.setConnectionRequestTimeout(connectionTimeout * 1000)
    			.setSocketTimeout(socketTimeout * 1000).build();
    	
    	cm = new PoolingHttpClientConnectionManager();
    	// Total max connections
    	cm.setMaxTotal( maxPoolSize );
    	// Default max connection per route
    	cm.setDefaultMaxPerRoute( minPoolSize );
    	
    	// Increase max connections for localhost:80 to 50
    	//HttpHost localhost = new HttpHost("locahost", 80);
    	//cm.setMaxPerRoute(new HttpRoute(localhost), 50);
    	
    	httpclient  = HttpClientBuilder.create()
    			.setRetryHandler(new DefaultHttpRequestRetryHandler( defaultRetryTimes , false))
    			.setDefaultRequestConfig(requestConfig)
    			.setConnectionManager(cm)
    			.build();
    	
    	
    }
    

    public String invoke(String serviceEndpoint, String xml) throws Exception {
    	
    	CloseableHttpResponse response = null;
    	
    	HttpPost httppost = new HttpPost(serviceEndpoint);
    	httppost.setHeader("Accept", "application/xml");
    	
    	try {
    		StringEntity entity = new StringEntity(xml);
    		entity.setContentType("application/xml");
    		//HttpEntity entity = new ByteArrayEntity(xml.getBytes("UTF-8"));
	    	httppost.setEntity(entity);
	    	//Execute and get the response.
	    	
	    	response = httpclient.execute(httppost);
	    	HttpEntity rEntity = response.getEntity();
	    
	    	
	    	return EntityUtils.toString(rEntity,"UTF-8");
    	} catch (Exception e) {
    		logger.error("Failed to send out request", e);
    		throw e;
    	} finally {
    		if (response != null) {
    			try {
					response.close();
				} catch (IOException e) {
					logger.error("Unable to close the response.", e);
				}
    		}
    	}
    }
    
    public void finalize() {
    	if (httpclient != null) {
    		try {
    			
				httpclient.close();
			} catch (IOException e) {
				logger.error("Unable to close the response.", e);
			}
    	}
    	
    	if (cm != null) {
    		
			cm.close();
			
    	}
    }
    
    
  
    
}
