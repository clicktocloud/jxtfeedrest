package com.ctc.jobboard.adapter;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpRequest;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

import com.ctc.jobboard.exception.JxtAccountException;
import com.ctc.jobboard.exception.JxtException;
import com.ctc.jobboard.jxt.component.Credential;
import com.ctc.jobboard.jxt.component.JxtAccount;
import com.ctc.jobboard.marshaller.MarshallerHelper;
import com.ctc.jobboard.util.Config;
import com.ctc.jobboard.util.SfOrg;
import com.ctc.salesforce.auth.SalesforceCredential;
import com.ctc.salesforce.enhanced.adaptor.SalesforceAgent;
import com.ctc.salesforce.exception.OauthDetailNotExistException;
import com.ctc.salesforce.service.SalesforceConnector;
import com.sforce.soap.partner.DescribeSObjectResult;
import com.sforce.soap.partner.Field;
import com.sforce.soap.partner.PartnerConnection;
import com.sforce.soap.partner.QueryResult;
import com.sforce.soap.partner.sobject.SObject;
import com.sforce.ws.ConnectionException;

public class SalesforceAdapter {
	private static Logger logger = Logger.getLogger("com.ctc.jobboard");
	
	
	
	public static String getOrgListQueryString(String jobBoardSfApiName){
		
		return "select SFDC_Org_ID__c, SF_UserName__c from Account " +
				 " where "+jobBoardSfApiName+"=true and PeopleCloud_Platform__c=true";
	}
	public static String getJobContentQueryString( String jobBoardName, String namespace){
		return "select Id, "+
				namespace+"Job_Posting_Status_txt__c, "+
				namespace+"Placement_Date__c, "+
				namespace+"JobXML__c, " + 
				namespace+"Job_Posting_Status__c " +
				" from "+
				namespace+"Advertisement__c " +
				" where "+
				namespace+"WebSite__c='"+jobBoardName+"' and "+
				namespace+"Status__c='Active'";
	}
	
	public static PartnerConnection getConnectionByOAuth(String orgId) throws ConnectionException{
		
		return SalesforceAgent.getConnectionByOAuth(orgId);
	}
	
	public static List<SfOrg>  getOrgList( String jobBoardSfApiName ) throws ConnectionException{
		
		PartnerConnection connection = getConnectionByOAuth(Config.CORP_ORG_ID);
		
		List<SfOrg> list = new ArrayList<SfOrg>();
		try {
			QueryResult qr = connection.query(  getOrgListQueryString( jobBoardSfApiName ) );
			boolean done = false;
			if(qr.getSize()>0){
				while(!done){
					for(SObject sobj: qr.getRecords()){
						
						//only for test nest org
						String orgId = (String)sobj.getField("SFDC_Org_ID__c");
						
						if(Config.isTest){
							if(Config.testOrgIds.contains(orgId)){
								list.add(new SfOrg(orgId));
							}
						}else{
							list.add(new SfOrg(orgId));
						}	
						
					}
					if(qr.isDone()){
						done = true;
					}else{
						qr = connection.queryMore(qr.getQueryLocator());
					}
				}
			}
		} catch (ConnectionException e) {
			logger.error("ConnectionException "+e);
			throw new JxtException("IOException "+e);
		}
		return list;

	}
	
	

	
	public static JxtAccount getJxtAccount(PartnerConnection connection, String jobBoardName, String namespace) throws JxtAccountException{
		
		if(connection == null){
			throw new JxtAccountException("Failed to connect to salesforce instance");
		}
		
		JxtAccount jxtAccount = getJxtAccountFromAdConfig(connection, jobBoardName, namespace);
		

		if(jxtAccount != null && ( StringUtils.isEmpty(jxtAccount.getUsername()) || StringUtils.isEmpty(jxtAccount.getPassword()) ) ){
			
			logger.debug("Set jxt account credential");
			setJxtAccountCredentialByRestful(connection, jxtAccount,namespace);
			
		}
		
		if(jxtAccount == null){
			throw new JxtAccountException("Failed to get jxt account");
		}
		
		
		
		return jxtAccount;
	}
	
	public static void setJxtAccountCredentialByRestful(PartnerConnection connection, JxtAccount jxtAccount,String namespace) throws JxtAccountException{
		
		
		String oauthToken = null;
		String serviceEndpoint = null;
		String orgId = null;
		try {
			orgId = connection.getUserInfo().getOrganizationId();
			
			SalesforceCredential credential = new SalesforceCredential();
	        credential.setPasswordLogin(false);
	        credential.setOrgId( orgId);
	        
			SalesforceConnector connector = new SalesforceConnector(credential);
			
			oauthToken = connector.getActiveSessionDto().getAccessToken();
			serviceEndpoint = connector.getActiveSessionDto().getInstanceUrl();
			
			
		} catch (ConnectionException | OauthDetailNotExistException e) {
			logger.error("Failed to pass oauth ! - "+ orgId);
			logger.error(e);
			throw new JxtAccountException("Failed to pass oauth ! - "+ orgId);
			
		}
		
		if(StringUtils.isEmpty(oauthToken) || StringUtils.isEmpty(serviceEndpoint)){
			logger.error("Failed to oauth token and service endpoint !");
			throw new JxtAccountException("Failed to oauth token and service endpoint for " + orgId);
			
			
		}
			
		try {
			Credential c = getProtectedCredential( serviceEndpoint, jxtAccount.getId(), oauthToken, namespace) ;
			
			logger.debug( c );
			if( c != null ){
				jxtAccount.setUsername( c.getUsername());
				jxtAccount.setPassword( c.getPassword() );
			}
		} catch (Exception e) {
			
			throw new JxtAccountException("Failed to get username and password for " + jxtAccount.getId());
		}
		
		
	}
	
	public static JxtAccount getJxtAccountFromAdConfig(PartnerConnection connection, String jobBoardName, String namespace) throws JxtAccountException{
		
		
		
		JxtAccount jxtAccount = null;
		if(connection == null){
			throw new JxtAccountException("Failed to connect to salesforce instance");
		}
		
		
		
		try{

			boolean isOldConfig = false;//objectHasField(connection, namespace+"Advertisement_Configuration__c", namespace+"UserName__c");
			
			logger.debug("isOldConfig =  " + isOldConfig );
			
			String queryString  =  "Select Id,"+
					namespace+"Advertisement_Account__c " +
					" From "+
					namespace+"Advertisement_Configuration__c " +
					" where "+
					namespace+"Advertisement_Account__c!=null and "+
					namespace+"Active__c=true and "+
					namespace+"WebSite__c='"+jobBoardName+"' limit 1";
			
			if( isOldConfig ){
				queryString  =  "Select Id,"+
						namespace+"Advertisement_Account__c, "+
						namespace+"UserName__c,  "+
						namespace+"Password__c " +
						" From "+
						namespace+"Advertisement_Configuration__c " +
						" where "+
						namespace+"Advertisement_Account__c!=null and "+
						//namespace+"UserName__c!=null and "+
						//namespace+"Password__c!=null and "+
						namespace+"Active__c=true and "+
						namespace+"WebSite__c='"+jobBoardName+"' limit 1";
			}
					
			QueryResult qr = connection.query(queryString);
			
			
			if(qr.getSize()>0){
				jxtAccount = new JxtAccount();
				
				SObject sobj = qr.getRecords()[0];
				
				jxtAccount.setId(sobj.getField("Id").toString());
				jxtAccount.setAdvertiserId(sobj.getField(namespace+"Advertisement_Account__c").toString());
				if( isOldConfig ){
					jxtAccount.setUsername((String)sobj.getField(namespace+"UserName__c"));
					jxtAccount.setPassword((String)sobj.getField(namespace+"Password__c"));
				}
				
			}else{
				throw new JxtAccountException("this org has no username, password or advertiser id for "+jobBoardName);
			}
		}catch(ConnectionException e){
			logger.error(e);
			throw new JxtAccountException("Failed to connect to salesforce instance");
		}
		
		return jxtAccount;
	}
	
	
	
	
	public  static boolean objectHasField(PartnerConnection connection,String sObjName, String fieldname){
		try {
			
			
			DescribeSObjectResult describeSObjectResult = connection.describeSObject(sObjName);
		
	        if (describeSObjectResult != null) {
	
	      
	        	Field[] fields = describeSObjectResult.getFields();
	
		        for (int i = 0; i < fields.length; i++) {
		
			          Field field = fields[i];
			          
			          if(field.getName().equalsIgnoreCase(fieldname)){
			        	  return true;
			          }
		        }
		       
	        }
			
	    } catch (Exception e) {
	    	logger.error(e);
	    }
		
		return false;

	}
	
	public static Credential getProtectedCredential(String serviceEndpoint, String name,String oauthToken,String namespace) throws Exception {
		
		Credential c = null;
    	
		String restname = "credential";
//		if(! StringUtils.isEmpty(Config.namespace)){
//			restname = Config.namespace.replace("__", "") + "/" +restname ;
//		}
		
		if(! StringUtils.isEmpty(namespace)){
			restname =namespace.replace("__", "") + "/" +restname ;
		}
		
    	CloseableHttpResponse response = null;
    	
    	String url = serviceEndpoint + "/services/apexrest/"+restname+"?name=" + URLEncoder.encode(name,"UTF-8");
    	
    	HttpGet httpget = new HttpGet(url);
    	
    	 Map<String, String> headers = new HashMap<String,String>();
    	 headers.put("Authorization", "Bearer " + oauthToken);
    	 headers.put("Accept", "application/json");
    	 
    	
    	setHttpRequestHeaders(httpget, headers);
    	
    	
    	try {
    		
	    	//Execute and get the response.
    		CloseableHttpClient httpclient  = HttpClients.createDefault();
	    	
	    	response = httpclient.execute(httpget);
	    	
	    	HttpEntity rEntity = response.getEntity();
	    	String j = EntityUtils.toString(rEntity,"UTF-8");
	    	
	    	if(response.getStatusLine().getStatusCode() == HttpStatus.SC_OK ){
	    		
	    		
	    		
	    		c = MarshallerHelper.convertJsontoObject(j, Credential.class);
	    	}else{
	    		logger.debug(j);
	    	}
   	
    	} catch (Exception e) {
    		logger.error("Failed to get protected credentials", e);
    		throw e;
    	} finally {
    		if (response != null ) {
    			try {
					response.close();
				} catch (IOException e) {
					logger.error("Unable to close the response.", e);
				}
    		}
    	}
    	
    	return c;
    }




	private static void setHttpRequestHeaders(HttpRequest request, Map<String, String> headers){
		if(headers != null && headers.size() > 0 ){
			for(Entry<String, String> header : headers.entrySet()){
				
				request.setHeader(header.getKey(), header.getValue());
				
			}	
		}
	}
	
}
