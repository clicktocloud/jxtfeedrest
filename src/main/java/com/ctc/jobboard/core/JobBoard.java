package com.ctc.jobboard.core;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.ctc.jobboard.jxt.domain.ext.JobPostResponse;
import com.ctc.jobboard.util.Config;
import com.ctc.jobboard.util.PartitionUtil;
import com.ctc.jobboard.util.SfOrg;
import com.sforce.soap.partner.PartnerConnection;
import com.sforce.soap.partner.SaveResult;
import com.sforce.soap.partner.sobject.SObject;
import com.sforce.ws.ConnectionException;

public abstract class JobBoard {
	static Logger logger = Logger.getLogger("com.ctc.jobboard");	
	private static final String  SF_POSTING_STATUS_ARCHIVED="Archived Successfully";
	private static final String  SF_POSTING_STATUS_SUCCESS="Posted Successfully";
	private static final String SF_POSTING_STATUS_UPDATED_SUCCESS = "Updated Successfully";
	private static final String  SF_POSTING_STATUS_FAILED="Posting Failed";
	private static final String  SF_POSTING_STATUS_UPDATE_FAILED="Update Failed";
	

	public static final String  SF_POSTING_STATUS_INQUEUE="In Queue";
	public static final String  SF_POSTING_STATUS_ARCHIVE_FAILED="Archiving Failed";
	
	
	public static final String  SF_AD_STATUS_ACTIVE="Active";
	public static final String  SF_AD_STATUS_INACTIVE="Inactive";
	public static final String  SF_AD_STATUS_ARCHIVED="Archived";
	
	
	private String namespace;
	protected String jobBoarId;
	protected PartnerConnection currentConnection;   
	protected String jobBoardName;
	protected String jobBoardSfApiName;
	protected Map<String, String> adStatusMap = new HashMap<String, String>();
	protected Map<String, String> adPostingStatuses = new HashMap<String, String>();
	
	protected SfOrg currentSforg; 
	protected List<SObject> updateSfList = new ArrayList<SObject>();
	
	public JobBoard(){
		namespace = Config.namespace;
	}
	
	public JobBoard(String jobBoardName, String jobBoardSfApiName){
		this();
		this.jobBoardName = jobBoardName;
		this.jobBoardSfApiName = jobBoardSfApiName;
	}
	

	public String getJobBoardName() {
		return jobBoardName;
	}

	public void setJobBoardName(String jobBoardName) {
		this.jobBoardName = jobBoardName;
	}

	public String getJobBoardSfApiName() {
		return jobBoardSfApiName;
	}

	public void setJobBoardSfApiName(String jobBoardSfApiName) {
		this.jobBoardSfApiName = jobBoardSfApiName;
	}

	public Map<String, String> getAdStatusMap() {
		return adStatusMap;
	}

	public void setAdStatusMap(Map<String, String> adStatusMap) {
		this.adStatusMap = adStatusMap;
	}

	

	public abstract void makeJobFeeds();
	
	public abstract void responseHandler();
	
	public abstract void post2JobBoard();
	
	protected void updateSFJobAds( ) throws ConnectionException{
		
		List<List<SObject>> sobjsList = PartitionUtil.partition(updateSfList, 200);
		for(List<SObject> sobjs : sobjsList){
			SaveResult[] srs = getCurrentConnection().update(sobjs.toArray(new SObject[]{}));
			for(SaveResult sr:srs){
				if(!sr.getSuccess()){
					for(com.sforce.soap.partner.Error error : sr.getErrors()){
						logger.error(error.getMessage()+" : "+Arrays.toString(error.getFields()));
					}
				}
			}
		}
		updateSfList.clear();
	}
	
	protected void addArchiveSObjects(String aid){
		SObject sobj = new SObject();
		sobj.setType(this.namespace +"Advertisement__c");
		sobj.setField("Id", aid);
		sobj.setField(this.namespace +"Job_Posting_Status__c", SF_POSTING_STATUS_ARCHIVED);
		
		//sobj.setField(this.namespace+"Status__c", SF_AD_STATUS_ARCHIVED);
		sobj.setField(this.namespace +"Ad_Posting_Status_Description__c", "Archived Successfully");
		
		updateSfList.add(sobj);
	}
	
	protected void addFailureSObjects(String aid, String failureMessage){
		SObject sobj = new SObject();
		sobj.setType(this.namespace +"Advertisement__c");
		sobj.setField("Id", aid);
		
		if(SF_POSTING_STATUS_INQUEUE.equals(adPostingStatuses.get(aid))){
			//sobj.setField(this.namespace+"Status__c", SF_AD_STATUS_INACTIVE);
			sobj.setField(this.namespace +"Job_Posting_Status__c", SF_POSTING_STATUS_FAILED);
		}else{
			sobj.setField(this.namespace +"Job_Posting_Status__c", SF_POSTING_STATUS_UPDATE_FAILED);
		}

		sobj.setField(this.namespace +"Ad_Posting_Status_Description__c", failureMessage);
		
		updateSfList.add(sobj);
	}
	
	
	
	protected void addSuccessSObjects(String aid, String jobBoardId, String onlineAdUrl,String onlineJobId,String operation){
		SObject sobj = new SObject();
		sobj.setType(this.namespace + "Advertisement__c");
		sobj.setField("Id", aid);
		sobj.setField(this.namespace + "Job_Board_ID__c", jobBoardId);
		if(JobPostResponse.UPDATE.equals(operation)){
			sobj.setField(this.namespace + "Job_Posting_Status__c", SF_POSTING_STATUS_UPDATED_SUCCESS);
			sobj.setField(this.namespace +"Ad_Posting_Status_Description__c", SF_POSTING_STATUS_UPDATED_SUCCESS);
		}else{
			sobj.setField(this.namespace + "Job_Posting_Status__c", SF_POSTING_STATUS_SUCCESS);
			sobj.setField(this.namespace +"Ad_Posting_Status_Description__c",  SF_POSTING_STATUS_SUCCESS);
			if(StringUtils.isBlank(getAdStatusMap().get(aid))){
				//Added the Calender, as the date when inserted in Salesforce and if the time is less than 10 AM then it converts the date to previous date.
				//So set the time to 11 AM to avoid the previous date issue.
				Calendar cal = Calendar.getInstance();  
		        cal.setTime(new Date());  
		        cal.set(Calendar.HOUR_OF_DAY, 11); 
				sobj.setField(this.namespace + "Placement_Date__c", cal.getTime());
			}
		}

		
		sobj.setField(this.namespace + "Online_Ad__c", onlineAdUrl);
		
		sobj.setField(this.namespace + "Online_Job_Id__c", onlineJobId);
		
		updateSfList.add(sobj);
	}

	public PartnerConnection getCurrentConnection() {
		return currentConnection;
	}

	public SfOrg getCurrentSforg() {
		return currentSforg;
	}

	public void setCurrentSforg(SfOrg currentSforg) {
		this.currentSforg = currentSforg;
	}

	public String getNamespace() {
		return namespace == null ? "" : namespace;
	}

	public void setNamespace(String namespace) {
		
		this.namespace = namespace == null ? "" : namespace;
	}

	public String getJobBoarId() {
		return jobBoarId;
	}

	public void setJobBoarId(String jobBoarId) {
		this.jobBoarId = jobBoarId;
	}
	
}
