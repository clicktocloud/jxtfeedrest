package com.ctc.jobboard.jxt.domain.ext;

import java.util.LinkedHashSet;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubClassificationList", propOrder = {
    "subClassification"
})
@XmlRootElement(name = "SubClassificationList")
public class SubClassificationList {
	
	@XmlElement(name = "SubClassification")
	private Set<SubClassification> subClassification;
	

	public Set<SubClassification> getSubClassification() {
		if(subClassification == null)
			subClassification = new LinkedHashSet<SubClassification>();
		return subClassification;
	}

	
	

}
