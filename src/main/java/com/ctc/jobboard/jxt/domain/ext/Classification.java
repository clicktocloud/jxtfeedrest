package com.ctc.jobboard.jxt.domain.ext;

import java.util.Comparator;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;



@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Classification", propOrder = {
    "id",
    "name",
    "subClassificationList"
})

public class Classification {
	
	private static final String LAST_CLASSIFICATION_NAME = "other";
	public static final String FIRST_CLASSIFICATION_NAME = "--None--";
	
	@XmlElement(name="Id")
	private String id;
	
	@XmlElement(name="Name")
	private String name;
	
	@XmlElement(name="SubClassificationList")
	private SubClassificationList subClassificationList;
	

	public Classification(String id, String name) {
		super();
		this.id = id;
		this.name = name;
	
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	
	public void setName(String name) {
		this.name = name;
	}

	

	public SubClassificationList getSubClassificationList() {
		return subClassificationList;
	}

	public void setSubClassificationList(SubClassificationList subClassificationList) {
		this.subClassificationList = subClassificationList;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Classification other = (Classification) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
	public static final Comparator<Classification> SORT_BY_ID = new Comparator<Classification>() {

		@Override
		public int compare(Classification o1, Classification o2) {
			
			if( o1 == null || o1.id == null){
				return -1;
			}
			return o1.id.compareTo(o2.id);
		}
		
	};
	
	public static final Comparator<Classification> SORT_BY_NAME = new Comparator<Classification>() {

		@Override
		public int compare(Classification o1, Classification o2) {
			
			if( o1 == null || o1.name == null){
				return -1;
			}
			
			if( o2 == null || o2.name == null){
				return 1;
			}
			
			if( o1.name.equalsIgnoreCase(FIRST_CLASSIFICATION_NAME)){
				return -1;
			}
			
			if( o2.name.equalsIgnoreCase(FIRST_CLASSIFICATION_NAME)){
				return 1;
			}
			
			if(LAST_CLASSIFICATION_NAME.equalsIgnoreCase(o2.name)){
				
				return -1;
			}
			if(LAST_CLASSIFICATION_NAME.equalsIgnoreCase(o1.name)){
				
				return 1;
			}
			
			return o1.name.toLowerCase().compareTo(o2.name.toLowerCase());
		}
		
	};

}
