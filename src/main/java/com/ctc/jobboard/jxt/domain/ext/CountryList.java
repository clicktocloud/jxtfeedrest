package com.ctc.jobboard.jxt.domain.ext;

import java.util.HashSet;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CountryList", propOrder = {
    "country"
})
@XmlRootElement(name = "CountryList")
public class CountryList {
	
	@XmlElement(name = "Country")
	private Set<Country> country;
	

	public Set<Country> getCountries() {
		if(country == null)
			country = new HashSet<Country>();
		return country;
	}

	
	

}
