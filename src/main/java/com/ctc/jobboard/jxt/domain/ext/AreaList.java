package com.ctc.jobboard.jxt.domain.ext;

import java.util.LinkedHashSet;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AreaList", propOrder = {
    "area"
})
@XmlRootElement(name = "AreaList")
public class AreaList {
	
	@XmlElement(name = "Area")
	private Set<Area> area;
	

	public Set<Area> getArea() {
		if(area == null)
			area = new LinkedHashSet<Area>();
		return area;
	}

	
	

}
