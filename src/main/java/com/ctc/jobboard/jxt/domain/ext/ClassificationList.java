package com.ctc.jobboard.jxt.domain.ext;

import java.util.LinkedHashSet;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ClassificationList", propOrder = {
    "classification"
})
@XmlRootElement(name = "ClassificationList")
public class ClassificationList {
	
	@XmlElement(name = "Classification")
	private Set<Classification> classification;
	

	public Set<Classification> getClassification() {
		if(classification == null)
			classification = new LinkedHashSet<Classification>();
		return classification;
	}

	
	

}
