package com.ctc.jobboard.jxt.domain.ext;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Area", propOrder = {
	"areaId",
	"areaName",
    "locationId",
    "countryId"
})


public class Area {
	
	
	@XmlElement(name="AreaId")
	private String areaId;
	
	@XmlElement(name="AreaName")
	private String areaName;

	@XmlElement(name="LocationId")
	private String locationId;
	
	@XmlElement(name="CountryId")
	private String countryId;
	
	
	
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((areaId == null) ? 0 : areaId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Area other = (Area) obj;
		if (areaId == null) {
			if (other.areaId != null)
				return false;
		} else if (!areaId.equals(other.areaId))
			return false;
		return true;
	}

	public Area(String areaId, String areaName, String locationId,
			String countryId) {
		super();
		this.areaId = areaId;
		this.areaName = areaName;
		this.locationId = locationId;
		this.countryId = countryId;
	}

	public String getAreaId() {
		return areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public String getCountryId() {
		return countryId;
	}

	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}
	
	
	

	
	
}
