package com.ctc.jobboard.jxt.domain.ext;

import java.util.LinkedHashSet;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LocationList", propOrder = {
    "location"
})
@XmlRootElement(name = "LocationList")
public class LocationList {
	
	@XmlElement(name = "Location")
	private Set<Location> location;
	

	public Set<Location> getLocation() {
		if(location == null)
			location = new LinkedHashSet<Location>();
		return location;
	}

	
	

}
