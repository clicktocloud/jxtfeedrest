package com.ctc.jobboard.jxt.service;



import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;

import com.ctc.jobboard.adapter.SalesforceAdapter;
import com.ctc.jobboard.exception.JxtException;
import com.ctc.jobboard.jxt.component.JXTDefaultListBoards;
import com.ctc.jobboard.jxt.domain.ArrayOfAdvertiserJobTemplateLogo;
import com.ctc.jobboard.jxt.domain.ArrayOfCountryLocationArea;
import com.ctc.jobboard.jxt.domain.ArrayOfJobTemplate;
import com.ctc.jobboard.jxt.domain.ArrayOfProfessionRole;
import com.ctc.jobboard.jxt.domain.ArrayOfWorkType;
import com.ctc.jobboard.jxt.domain.CountryLocationArea;
import com.ctc.jobboard.jxt.domain.ProfessionRole;
import com.ctc.jobboard.jxt.domain.ext.AdvertiserJobTemplateLogoList;
import com.ctc.jobboard.jxt.domain.ext.Area;
import com.ctc.jobboard.jxt.domain.ext.AreaList;
import com.ctc.jobboard.jxt.domain.ext.Classification;
import com.ctc.jobboard.jxt.domain.ext.ClassificationList;
import com.ctc.jobboard.jxt.domain.ext.Country;
import com.ctc.jobboard.jxt.domain.ext.CountryList;
import com.ctc.jobboard.jxt.domain.ext.JobTemplateList;
import com.ctc.jobboard.jxt.domain.ext.Location;
import com.ctc.jobboard.jxt.domain.ext.LocationList;
import com.ctc.jobboard.jxt.domain.ext.SubClassification;
import com.ctc.jobboard.jxt.domain.ext.SubClassificationList;
import com.ctc.jobboard.jxt.domain.ext.WorkTypeList;
import com.ctc.jobboard.util.SfOrg;
import com.sforce.ws.ConnectionException;

public class JXTGetDefaultListService extends JXTDefaultListBoards{
	
	static Logger logger = Logger.getLogger("com.ctc.jobboard");
	
	private static final String NONE_VALUE="";
	private static final String NONE_NAME="--None--";

	
	public JXTGetDefaultListService(String jobBoardName, String namespace) {
		super(jobBoardName, "");
		setNamespace(namespace);
		
	}

	public JXTGetDefaultListService retrieveDefaultList(String orgId ) throws JxtException{
		if(orgId == null ){
			logger.error("Org Id can not be empty !");
			throw new JxtException("Org Id can not be empty !");
		}
		
		try {
			currentSforg = new SfOrg(orgId);
			currentConnection = SalesforceAdapter.getConnectionByOAuth( orgId);

			makeJobFeeds();
			
		} catch (ConnectionException e) {
			logger.error("Failed to connect to salesforce instance of org ["+ orgId +"]", e);
			throw new JxtException("Failed to connect to salesforce instance of org ["+ orgId +"]", e);
		}
		
		return this;
	
	}
	

	public JXTGetDefaultListService retrieveDefaultList( String advertiserId, String username, String password ) throws JxtException{
		
		setDefaultJxtAccount( buildJxtAccount(advertiserId, username, password) );
		
		makeJobFeeds();
		
		return this;
		
	
	}
	
	
	public JobTemplateList getJobTemplateList(){
		JobTemplateList list = new JobTemplateList();
		if( getResponse() != null && getResponse().getDefaultList() != null){
			ArrayOfJobTemplate jt = getResponse().getDefaultList().getJobTemplates();
			if(jt != null && jt.getJobTemplate() != null){
				list.getJobTemplate().addAll(jt.getJobTemplate());
			}
			
		}
		
		return list;
	}
	
	public WorkTypeList getWorkTypeList(){
		WorkTypeList list = new WorkTypeList();
		if( getResponse() != null && getResponse().getDefaultList() != null){
			ArrayOfWorkType wt = getResponse().getDefaultList().getWorkTypes();
			if( wt != null && wt.getWorkType() != null){
				list.getWorkType().addAll( wt.getWorkType() );
			}
		}
		
		return list;
	}
	
	public AdvertiserJobTemplateLogoList getAdvertiserJobTemplateLogoList(){
		AdvertiserJobTemplateLogoList list = new AdvertiserJobTemplateLogoList();
		if( getResponse() != null && getResponse().getDefaultList() != null){
			ArrayOfAdvertiserJobTemplateLogo tl = getResponse().getDefaultList().getAdvertiserJobTemplateLogos();
			if( tl != null && tl.getAdvertiserJobTemplateLogo()!= null){
				list.getAdvertiserJobTemplateLogo().addAll( tl.getAdvertiserJobTemplateLogo() );
			}
		}
		
		return list;
	}
	
	public CountryList getCountryList(){
		CountryList countryList = new CountryList();
		if( getResponse() != null && getResponse().getDefaultList() != null){
			ArrayOfCountryLocationArea clas = getResponse().getDefaultList().getCountryLocationAreas();
			if(clas != null && clas.getCountryLocationArea() != null){
				for( CountryLocationArea cla : clas.getCountryLocationArea()){
					
					Country c = new Country(cla.getCountryID(), cla.getCountryName());
					c.setLocationList( getLocationList( c.getCountryId() ));
					countryList.getCountries().add(c );
					
				}
			}
		}
		
		return countryList;
	}
	
	public LocationList getLocationList(){
		LocationList locationList = new LocationList();
		if( getResponse() != null && getResponse().getDefaultList() != null){
			ArrayOfCountryLocationArea clas = getResponse().getDefaultList().getCountryLocationAreas();
			if(clas != null && clas.getCountryLocationArea() != null){
				for( CountryLocationArea cla : clas.getCountryLocationArea()){
					
					Location l = new Location(cla.getLocationID(), cla.getLocationName(),cla.getCountryID());
					l.setAreaList( getAreaList( l.getLocationId() ));
					locationList.getLocation().add( l );
					
				}
			}
		}
		
		return locationList;
	}
	
	public LocationList getLocationList(String countryId){
		LocationList locationList = new LocationList();
		if(countryId == null)
			return locationList;
		
		if( getResponse() != null && getResponse().getDefaultList() != null){
			ArrayOfCountryLocationArea clas = getResponse().getDefaultList().getCountryLocationAreas();
			if(clas != null && clas.getCountryLocationArea() != null){
				
				Location none =  new Location(NONE_VALUE, NONE_NAME,countryId);
				locationList.getLocation().add(none);
				
				none.setAreaList(getAreaList( NONE_VALUE  ));
				
				for( CountryLocationArea cla : clas.getCountryLocationArea()){
					if(countryId.equals(cla.getCountryID())){
						
						Location l = new Location(cla.getLocationID(), cla.getLocationName(),cla.getCountryID());
						l.setAreaList( getAreaList( l.getLocationId() ));
						
						locationList.getLocation().add(l);
					}	
				}
			}
		}
		
		return locationList;
	}
	
	
	public AreaList getAreaList(){
		AreaList areaList = new AreaList();
		if( getResponse() != null && getResponse().getDefaultList() != null){
			ArrayOfCountryLocationArea clas = getResponse().getDefaultList().getCountryLocationAreas();
			if(clas != null && clas.getCountryLocationArea() != null){
				for( CountryLocationArea cla : clas.getCountryLocationArea()){
					areaList.getArea().add(new Area(cla.getAreaID(), cla.getAreaName(),cla.getLocationID(), cla.getCountryID()));
					
				}
			}
		}
		
		return areaList;
	}
	
	public AreaList getAreaList(String locationId){
		
		AreaList areaList = new AreaList();
		if(locationId == null)
			return areaList;
		
		if(locationId.equals(NONE_VALUE)){
			areaList.getArea().add(new Area(NONE_VALUE, NONE_NAME,NONE_VALUE, NONE_VALUE));
			return areaList;
		}
		
		if( getResponse() != null && getResponse().getDefaultList() != null){
			ArrayOfCountryLocationArea clas = getResponse().getDefaultList().getCountryLocationAreas();
			if(clas != null && clas.getCountryLocationArea() != null){
				areaList.getArea().add(new Area(NONE_VALUE, NONE_NAME,locationId, NONE_VALUE));
				for( CountryLocationArea cla : clas.getCountryLocationArea()){
					if(locationId.equals(cla.getLocationID())){
						areaList.getArea().add(new Area(cla.getAreaID(), cla.getAreaName(),cla.getLocationID(), cla.getCountryID()));
					}
					
					
				}
			}
		}
		
		return areaList;
	}
	
	public ClassificationList getClassificationList(){
		ClassificationList list = new ClassificationList();
		if( getResponse() != null && getResponse().getDefaultList() != null){
			ArrayOfProfessionRole prs = getResponse().getDefaultList().getProfessionRoles();
			if(prs != null && prs.getProfessionRole() != null){
				
				List<Classification> clist = new ArrayList<Classification>();
				
				Classification none = new Classification(NONE_VALUE, NONE_NAME);
				none.setSubClassificationList(getSubClassificationList( NONE_VALUE ));
				clist.add(none);
				
				for( ProfessionRole pr : prs.getProfessionRole()){
					
					Classification c = new Classification(pr.getProfessionID(), pr.getProfessionName());
					c.setSubClassificationList( getSubClassificationList( c.getId() ) ) ;
					
					//list.getClassification().add(c);
					clist.add(c);
				}
				
				
				Collections.sort(clist,Classification.SORT_BY_NAME);
				list.getClassification().addAll(clist);
			}
		}
		
		return list;
	}
	
	public SubClassificationList getSubClassificationList(){
		SubClassificationList list = new SubClassificationList();
		if( getResponse() != null && getResponse().getDefaultList() != null){
			ArrayOfProfessionRole prs = getResponse().getDefaultList().getProfessionRoles();
			if(prs != null && prs.getProfessionRole() != null){
				List<SubClassification> sclist = new ArrayList<SubClassification>();
				
				for( ProfessionRole pr : prs.getProfessionRole()){
					sclist.add(new SubClassification(pr.getRoleID(), pr.getRoleName(), pr.getProfessionID()));
					
				}
				
				Collections.sort(sclist,SubClassification.SORT_BY_NAME);
				list.getSubClassification().addAll(sclist);
			}
		}
		
		return list;
	}
	
	public SubClassificationList getSubClassificationList(String parentId){
		SubClassificationList list = new SubClassificationList();
		if(parentId == null)
			return list;
		
		if(parentId.equals(NONE_VALUE)){
			list.getSubClassification().add(new SubClassification(NONE_VALUE, NONE_NAME, NONE_VALUE));
			return list;
		}
		
		if( getResponse() != null && getResponse().getDefaultList() != null){
			ArrayOfProfessionRole prs = getResponse().getDefaultList().getProfessionRoles();
			if(prs != null && prs.getProfessionRole() != null){
				
				List<SubClassification> sclist = new ArrayList<SubClassification>();
				sclist.add(new SubClassification(NONE_VALUE, NONE_NAME, NONE_VALUE));
				for( ProfessionRole pr : prs.getProfessionRole()){
					if(parentId.equals(pr.getProfessionID())){
						sclist.add(new SubClassification(pr.getRoleID(), pr.getRoleName(), pr.getProfessionID()));
						//list.getSubClassification().add(new SubClassification(pr.getRoleID(), pr.getRoleName(), pr.getProfessionID()));
					}
				}
				Collections.sort(sclist,SubClassification.SORT_BY_NAME);
				list.getSubClassification().addAll(sclist);
			}
		}
		
		return list;
	}


	

	@Override
	public void makeJobFeeds() throws JxtException{
		try {
			
			
			
			logger.debug("Begin to get default list from job board["+jobBoardName+"]");
	
			post2JobBoard();
			
			logger.debug("-----------------------------------------");
			
			responseHandler();
			
		} catch (Exception e) {
			logger.error("Failed to get default list  "+e);
			
			throw new JxtException("Failed to get default list ",e);
		}
	}
	
	

}
