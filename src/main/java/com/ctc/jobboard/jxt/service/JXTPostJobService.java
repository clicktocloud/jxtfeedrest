package com.ctc.jobboard.jxt.service;



import javax.xml.bind.JAXBException;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.ctc.jobboard.adapter.SalesforceAdapter;
import com.ctc.jobboard.exception.JxtException;
import com.ctc.jobboard.jxt.component.JXTJobPostBoards;
import com.ctc.jobboard.jxt.domain.APIReturn;
import com.ctc.jobboard.jxt.domain.JobListing;
import com.ctc.jobboard.jxt.domain.ext.JobPostResponse;
import com.ctc.jobboard.marshaller.MarshallerHelper;
import com.ctc.jobboard.util.SfOrg;

public class JXTPostJobService extends JXTJobPostBoards{
	
	static Logger logger = Logger.getLogger("com.ctc.jobboard");
	
	private JobListing jobListing;
	private String referenceNo;

	
	public JXTPostJobService(String jobBoardName, String jobBoardSfApiName) {
		super(jobBoardName, jobBoardSfApiName);
		
	}
	
	public JobPostResponse postJob(String orgId, String referenceNo, String postingStatus, String jobXmlContent,String advertiserId, String username, String password, String namespace) throws JxtException{
		
		
		setDefaultJxtAccount( buildJxtAccount(advertiserId, username, password));
		
		setNamespace(namespace);
		
		return postJob(orgId, referenceNo, postingStatus, jobXmlContent);
	
	}

	public JobPostResponse postJob(String orgId, String referenceNo,String postingStatus, String jobXmlContent) throws JxtException{
		if(orgId == null || referenceNo==null || jobXmlContent == null){
			logger.error("Org Id and job content can not be empty !");
			throw new JxtException("Org Id, reference No and job content can not be empty !");
		}

		this.referenceNo =  referenceNo;
		currentSforg = new SfOrg(orgId);
		
		
		try {
			
			//jobXmlContent = JobBoardHelper.removeBadUTF8Chars(jobXmlContent);
			
			jobListing = MarshallerHelper.convertXMLtoObject(jobXmlContent, JobListing.class);
			String aid = JXTJobPostBoards.getAdId(referenceNo);
			if(aid != null){
				adPostingStatuses.put(aid, postingStatus);
			}
			
			makeJobFeeds();
			
		} catch (JAXBException e) {
			logger.error("JAXBException - Invalid job content !", e);
			
			JobPostResponse response = createJobPostResponse(APIReturn.ERROR,"Invalid job content","ERROR", "Invalid job content", this.referenceNo);
			
			setResponse(response);
		} catch (JxtException e){
			logger.error("JxtException - ", e);
			
			JobPostResponse response = createJobPostResponse(APIReturn.ERROR,e.getMessage(),"ERROR", e.getMessage(), this.referenceNo);
			
			setResponse(response);
		}
		
		logger.debug("Begin to handler response");
		
		responseHandler();
		
		logger.debug("-----------------------------------------");
		
		return getResponse();
	
	}
	
	public JobPostResponse postJsonJob(String orgId, String referenceNo, String postingStatus, JobListing jobListing,String advertiserId, String username, String password, String namespace) throws JxtException{
		
		setDefaultJxtAccount( buildJxtAccount(advertiserId, username, password));
		
		setNamespace(namespace);
		
		return postJsonJob(orgId, referenceNo,postingStatus, jobListing);
	
	}
	
	public JobPostResponse postJsonJob(String orgId, String referenceNo, String postingStatus, JobListing jobListing) throws JxtException{
		if(orgId == null || referenceNo==null || jobListing == null){
			logger.error("Org Id and job content can not be empty !");
			throw new JxtException("Org Id, reference No and job content can not be empty !");
		}

		currentSforg = new SfOrg(orgId);
		this.referenceNo =  referenceNo;
		
		try {

			String aid = JXTJobPostBoards.getAdId(referenceNo);
			if(! StringUtils.isEmpty( aid )){
				adPostingStatuses.put(aid, postingStatus);
			}
			
			
			makeJobFeeds();
			
		}  catch (JxtException e){
			logger.error("JxtException - ", e);
			
			JobPostResponse response = createJobPostResponse(APIReturn.ERROR,e.getMessage(),"ERROR", e.getMessage(), this.referenceNo);
			
			setResponse(response);
		}
		
		
		logger.debug("Begin to handler response");
		
		responseHandler();
		
		logger.debug("-----------------------------------------");
		
		return getResponse();
	
	}
	
	public JobPostResponse postJsonJob(String orgId, String referenceNo, String postingStatus, String jobJsonContent) throws JxtException{
		if(orgId == null || referenceNo==null || jobJsonContent == null){
			logger.error("Org Id and job content can not be empty !");
			throw new JxtException("Org Id, reference No and job content can not be empty !");
		}

		currentSforg = new SfOrg(orgId);
		this.referenceNo =  referenceNo;
		
		try {
			
			//jobJsonContent = JobBoardHelper.removeBadUTF8Chars(jobJsonContent);
			jobListing = MarshallerHelper.convertJsontoObject(jobJsonContent, JobListing.class);
			String aid = JXTJobPostBoards.getAdId(referenceNo);
			if(! StringUtils.isEmpty( aid )){
				adPostingStatuses.put(aid, postingStatus);
			}
			
			
			makeJobFeeds();
			
		} catch (JAXBException e) {
			logger.error("JAXBException - Invalid job content !", e);
			
			JobPostResponse response = createJobPostResponse(APIReturn.ERROR,"Invalid job content","ERROR", "Invalid job content", this.referenceNo);
			
			setResponse(response);
		} catch (JxtException e){
			logger.error("JxtException - ", e);
			
			JobPostResponse response = createJobPostResponse(APIReturn.ERROR,e.getMessage(),"ERROR", e.getMessage(), this.referenceNo);
			
			setResponse(response);
		}
		
		
		logger.debug("Begin to handler response");
		
		responseHandler();
		
		logger.debug("-----------------------------------------");
		
		return getResponse();
	
	}
	
	
	
	public JobPostResponse postJsonJob(String orgId, String referenceNo, String postingStatus, String jobJsonContent,String advertiserId, String username, String password, String namespace) throws JxtException{
		
		setDefaultJxtAccount( buildJxtAccount(advertiserId, username, password));
		
		setNamespace(namespace);
		
		return postJsonJob(orgId, referenceNo,postingStatus, jobJsonContent);
	
	}

	@Override
	public void makeJobFeeds() throws JxtException{
		if( jobListing == null){
			return;
			
		}
		try {
			

			logger.debug("Begin to make job listing ["+referenceNo+"] for org ["+currentSforg.getOrgId()+"]");
			
			
			jobListings.getJobListing().add(jobListing);
			
			
			logger.debug("-----------------------------------------");
			
			logger.debug("Connecting to Salesforce instance of org ["+currentSforg.getOrgId()+"]");
			
			currentConnection = SalesforceAdapter.getConnectionByOAuth( currentSforg.getOrgId() );
			
			logger.debug("-----------------------------------------");
			
			//
			logger.debug("Begin to post job ["+referenceNo+"] to job board["+jobBoardName+"]");
			
			post2JobBoard();
			
			logger.debug("-----------------------------------------");
			
			throwJxtExceptionFromUnsuccessfulJobPostResponse();
			
		}  catch (JxtException e) {
			logger.error(e);
			throw e;
		} catch (Exception e) {
			logger.error("Exception :referenceNo="+referenceNo+" orgId="+currentSforg.getOrgId()+"  "+e);
			throw new JxtException("Other error - ",e);
		}
	}

}
