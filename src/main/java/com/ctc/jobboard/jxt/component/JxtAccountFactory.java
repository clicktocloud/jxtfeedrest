package com.ctc.jobboard.jxt.component;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.ctc.jobboard.adapter.SalesforceAdapter;
import com.ctc.jobboard.exception.JxtAccountException;
import com.sforce.soap.partner.PartnerConnection;
import com.sforce.ws.ConnectionException;

public class JxtAccountFactory {
	static Logger logger = Logger.getLogger("com.ctc.jobboard");
	
	private static Map<String, JxtAccount> accounts = new HashMap<String, JxtAccount>();
	
	public static void addJxtAccount(String orgId,String jobBoardName, String advertiserId, String username, String password){
		
		if(orgId == null || jobBoardName == null){
			return;
		}
		
		JxtAccount a = new JxtAccount();
		a.setAdvertiserId(advertiserId);
		a.setPassword(password);
		a.setUsername(username);
		
		synchronized (accounts) {
			accounts.put(orgId + ":" + jobBoardName, a);
		}
		
	}
	public static JxtAccount getJxtAccount(String orgId, String jobBoardName, String namespace) throws JxtAccountException{
		
		synchronized (accounts) {
			String key = orgId + ":" + jobBoardName;
			JxtAccount a = accounts.get(key);
			if (a == null) {

				try{
					PartnerConnection conn = SalesforceAdapter
							.getConnectionByOAuth(orgId);

					a = SalesforceAdapter.getJxtAccount(conn, jobBoardName, namespace);
					if(a != null)
						accounts.put(key, a);
				}catch(ConnectionException e){
					logger.error("Failed to connect to salesforce instance of org["+orgId+"]");
					throw new JxtAccountException("Failed to connect to salesforce instance of org["+orgId+"]");
				}
			}
			return a;
		}

	}
	
	public static JxtAccount getJxtAccount(PartnerConnection conn, String orgId, String jobBoarName, String namespace) throws JxtAccountException{
		
		synchronized (accounts) {
			String key = orgId + ":" + jobBoarName;
			JxtAccount a = accounts.get(key);
			if (a == null) {

				a = SalesforceAdapter.getJxtAccount(conn, jobBoarName, namespace);
				
				if( a != null)
					accounts.put(key, a);
				
			}
			return a;
		}
	}
	
	
	

}
