package com.ctc.jobboard.jxt.component;

import org.apache.log4j.Logger;

import com.ctc.jobboard.exception.BadRequestException;
import com.ctc.jobboard.exception.JxtAccountException;
import com.ctc.jobboard.jxt.domain.DefaultRequest;
import com.ctc.jobboard.jxt.domain.DefaultResponse;

public abstract class JXTDefaultListBoards extends JXTJobBoard{
	
	static Logger logger = Logger.getLogger("com.ctc.jobboard");
	
	
	protected DefaultResponse response;  
	
	
	
	/**
	 * Constructor 
	 * 
	 * @param jobBoardName
	 * @param jobBoardSfApiName
	 */
	protected JXTDefaultListBoards(String jobBoardName, String jobBoardSfApiName) {
		super(jobBoardName, jobBoardSfApiName);
	}
	
	
	@Override
	public void post2JobBoard() {
		
		DefaultRequest request = new DefaultRequest();
		
		try {
			JxtAccount jxtAccount = getJxtAccount();
			
			logger.debug("AdvertiserId = "+jxtAccount.getAdvertiserId());
			logger.debug("username = "+jxtAccount.getUsername());
			logger.debug("password = "+jxtAccount.getPassword());
			
			//set credentials
			request.setAdvertiserId( jxtAccount.getIntAdvertiserId());
			request.setUserName(jxtAccount.getUsername());
			request.setPassword(jxtAccount.getPassword());

			DefaultResponse response = execute(request,DefaultResponse.class);
			
			
			setResponse( response );
			
		} catch (JxtAccountException e) {
			logger.error(e);
		} catch (BadRequestException e) {
			logger.error("Failed to call JXT web service ! ", e);
		} catch( Exception e){
			logger.error("Unknown error ", e);
		}
	}
	
	@Override
	public void responseHandler(){
		
		if( getResponse() != null ){
			
			logger.debug("Code : " +getResponse().getResponseCode());
			logger.debug("Message : " +getResponse().getResponseMessage());
			
		}
		
	}
	
	
	
	
	public DefaultResponse getResponse() {
		return response;
	}

	public void setResponse(DefaultResponse response) {
		this.response = response;
	}
	
	

	

	

}
