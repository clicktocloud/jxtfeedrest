package com.ctc.jobboard.jxt.component;

import org.apache.log4j.Logger;

import com.ctc.jobboard.core.JobBoard;
import com.ctc.jobboard.exception.BadRequestException;
import com.ctc.jobboard.exception.JxtAccountException;
import com.ctc.jobboard.exception.JxtException;
import com.ctc.jobboard.jxt.domain.ArchiveJobRequest;
import com.ctc.jobboard.jxt.domain.ext.ArrayOfJob;
import com.ctc.jobboard.jxt.domain.ext.JobPostResponse;
import com.sforce.soap.partner.sobject.SObject;

public abstract class JXTJobArchiveBoards extends JXTJobPostBoards{
	
	static Logger logger = Logger.getLogger("com.ctc.jobboard");
	
	
	protected ArrayOfJob archiveJobListings = new ArrayOfJob();
	
	/**
	 * Constructor 
	 * 
	 * @param jobBoardName
	 * @param jobBoardSfApiName
	 */
	protected JXTJobArchiveBoards(String jobBoardName, String jobBoardSfApiName) {
		super(jobBoardName, jobBoardSfApiName);
	}
	
	
	@Override
	public void post2JobBoard() {
		
		ArchiveJobRequest request = new ArchiveJobRequest();
		
		
		
		try {
			JxtAccount jxtAccount = getJxtAccount( );
			
			logger.debug("AdvertiserId = "+jxtAccount.getAdvertiserId());
			logger.debug("username = "+jxtAccount.getUsername());
			logger.debug("password = "+jxtAccount.getPassword());
			
			//set credentials
			request.setAdvertiserId( jxtAccount.getIntAdvertiserId());
			request.setUserName(jxtAccount.getUsername());
			request.setPassword(jxtAccount.getPassword());
		
			
			//
			ArrayOfJob listings = getArchiveJobListings();
			if( listings != null && listings.getJob().size() > 0){
				request.setListings(listings);
				
				JobPostResponse response = execute(request, JobPostResponse.class);
				
				setResponse( response );
			}else{
				logger.info("No any job to be posted from org org ["+getCurrentSforg().getOrgId()+"]");
			}
		} catch (JxtAccountException e) {
			
			throw new JxtException("JxtAccountException - " + e.getMessage());
		} catch (BadRequestException e) {
			
			throw new JxtException("BadRequestException - " + e.getMessage());
		} catch( Exception e){
			
			throw new JxtException("Other error - " + e.getMessage());
		}
	}
	
	public ArrayOfJob getArchiveJobListings() {
		return archiveJobListings;
	}

	protected void addFailureSObjects(String aid, String failureMessage){
		SObject sobj = new SObject();
		sobj.setType(getNamespace() +"Advertisement__c");
		sobj.setField("Id", aid);
		
		sobj.setField(getNamespace()+"Status__c", JobBoard.SF_AD_STATUS_ACTIVE);
		
		sobj.setField(getNamespace() +"Job_Posting_Status__c", JobBoard.SF_POSTING_STATUS_ARCHIVE_FAILED);
		
		sobj.setField(getNamespace() +"Ad_Posting_Status_Description__c", failureMessage);
		
		updateSfList.add(sobj);
	}
	

}
