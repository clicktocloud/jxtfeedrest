package com.ctc.jobboard.jxt.component;

public class JxtAccount {
	
	
	private String id;
	private String username;
	private String password;
	private String advertiserId;
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getAdvertiserId() {
		if(advertiserId==null)
			advertiserId="0";
			
		return advertiserId;
	}
	public void setAdvertiserId(String advertiserId) {
		this.advertiserId = advertiserId;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public JxtAccount(String username, String password, String advertiserId) {
		super();
		this.username = username;
		this.password = password;
		this.advertiserId = advertiserId;
	}
	
	public Integer getIntAdvertiserId(){
		Integer id = 0;
		
		try {
			id = Integer.valueOf(getAdvertiserId());
		} catch (NumberFormatException e) {
			
		}
		
		return id;
	}
	
	public JxtAccount(){}
	@Override
	public String toString() {
		return "JxtAccount [id=" + id + ", username=" + username
				+ ", password=" + password + ", advertiserId=" + advertiserId
				+ "]";
	}
}
