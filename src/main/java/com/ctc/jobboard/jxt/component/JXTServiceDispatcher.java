package com.ctc.jobboard.jxt.component;


import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.JAXBException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ctc.jobboard.adapter.RestHttpClient;
import com.ctc.jobboard.exception.BadRequestException;
import com.ctc.jobboard.jxt.domain.ArchiveJobRequest;
import com.ctc.jobboard.jxt.domain.BaseRequest;
import com.ctc.jobboard.jxt.domain.ConsultantRequest;
import com.ctc.jobboard.jxt.domain.DefaultRequest;
import com.ctc.jobboard.jxt.domain.JobPostRequest;
import com.ctc.jobboard.marshaller.MarshallerHelper;


public class JXTServiceDispatcher {
	
	private static final String SERVICE_DOMAIN = "http://webservices.jxt.net.au";
	
	private static Map<Class<?>, String> requestEndpoints = new HashMap<Class<?>, String>();
	static{
		requestEndpoints.put(JobPostRequest.class, SERVICE_DOMAIN +"/Post/Jobs");
		requestEndpoints.put(DefaultRequest.class, SERVICE_DOMAIN +"/Get/DefaultList");
		requestEndpoints.put(ConsultantRequest.class, SERVICE_DOMAIN +"/Get/Consultants");
		requestEndpoints.put(ArchiveJobRequest.class, SERVICE_DOMAIN +"/Archive/Jobs");
	}
	
	private RestHttpClient rsClient;
	
	 
	
	private Logger logger = LoggerFactory.getLogger(JXTServiceDispatcher.class);
	
	public JXTServiceDispatcher( ) {
		 
		this.setRsClient(new RestHttpClient());
	}
	
	
	public JXTServiceDispatcher(int minPoolSize, int maxPoolSize, int connectionTimeout, int socketTimeout) {
		
		this.setRsClient(new RestHttpClient(minPoolSize, maxPoolSize, connectionTimeout, socketTimeout));
	}
	
	
	
	
	public <R> R execute(BaseRequest request, Class<R> clazz) throws BadRequestException {
		
		String bodyString = null;
		String xmlResult = "";
    	try {
    		bodyString = MarshallerHelper.convertObjecttoXML(request);
    	
    		//System.out.println(bodyString);
    		String serviceEndpoint = requestEndpoints.get(request.getClass());
    		//System.out.println(serviceEndpoint);
    		if(serviceEndpoint != null){
    			long begin = System.currentTimeMillis();
    			xmlResult = rsClient.invoke( serviceEndpoint, bodyString);
    			logger.debug("Response from JXT - timecost: ["+(System.currentTimeMillis() - begin)+" ms]");
    			
    			//System.out.println(xmlResult);
    			
    			if(!xmlResult.startsWith("<") && xmlResult.indexOf("<") >= 0){
    				xmlResult = xmlResult.substring(xmlResult.indexOf("<"));
    			}
    			
        		R r = MarshallerHelper.convertXMLtoObject(xmlResult, clazz);
        		
        		return r;
    			
    		}else{
    			throw new BadRequestException(-1, "Invalid request type - no service endpoint found !");
    		}
    		
    		
    	}catch(BadRequestException e){
    		
    		logger.error(e.getMessage());
    		
    		throw e;
    	}  catch(JAXBException e){
    		e.printStackTrace();
    		logger.error("Parse request object failed.", e);
    		logger.error(xmlResult);
    		throw new BadRequestException(-2, "Parse request/response object failed !");
    	} catch (Exception e) {
    		
    		logger.error("Unknown error." + e.getMessage(), e);
    		//logger.debug( bodyString );
    		throw new BadRequestException(-3, "Unknown error." + e.getMessage());
    	}
	}

	public RestHttpClient getRsClient() {
		return rsClient;
	}

	public void setRsClient(RestHttpClient rsClient) {
		this.rsClient = rsClient;
	}
}
