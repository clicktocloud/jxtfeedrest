package com.ctc.jobboard.jxt.component;

public class Credential {
	
	public static final String PROVIDER_LINKEDIN = "linkedin";
	public static final String PROVIDER_INDEED = "indeed";
	public static final String PROVIDER_JXT = "JXT";
	public static final String PROVIDER_CAREERONE = "careerone";
	public static final String PROVIDER_ASTUTE = "astute";
	public static final String PROVIDER_AMAZON = "amazon";
	
	public static final String CREDENTIAL_AD_LINKEDIN = "linkedin"; 
	public static final String CREDENTIAL_AD_INDEED = "indeed"; 
	public static final String CREDENTIAL_AD_JXT_NZ = "JXT_NZ"; 
	public static final String CREDENTIAL_AD_CAREERONE = "careerone"; 
	public static final String CREDENTIAL_PAYROLL_ASTUTE = "astutepayroll"; 
	public static final String CREDENTIAL_SQS_AMAZON = "amazonsqs"; 
	
	
	public static final String TYPE_ADVERTISEMENT = "advertisement"; 
	public static final String TYPE_PAYROLL = "payroll"; 
	public static final String TYPE_SQS = "sqs"; 
	
	
	private String orgId;
	private String Id;
	private String name;
	private String type;//advertisement 
	private String provider;
	private String username;
	private String password;
	private String token;
	private String key;
	private String secret;
	private String relatedTo;
	
	public Credential(){
		
	}
	
	public Credential(String orgId, String name, String type,String provider){
		this.orgId = orgId;
		this.name  = name;
		this.type = type;
		this.provider = provider;
		
	}
	public String getOrgId() {
		return orgId;
	}
	public String getName() {
		return name;
	}
	public String getType() {
		return type;
	}
	public String getProvider() {
		return provider;
	}
	
	public String getUsername() {
		return username;
	}
	public String getPassword() {
		return password;
	}
	public String getToken() {
		return token;
	}
	public String getKey() {
		return key;
	}
	public String getSecret() {
		return secret;
	}
	
	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setType(String type) {
		this.type = type;
	}
	public void setProvider(String provider) {
		this.provider = provider;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public void setSecret(String secret) {
		this.secret = secret;
	}

	public String getRelatedTo() {
		return relatedTo;
	}

	public void setRelatedTo(String relatedTo) {
		this.relatedTo = relatedTo;
	}

	public String getId() {
		return Id;
	}

	public void setId(String id) {
		Id = id;
	}

	@Override
	public String toString() {
		return "Credential [orgId=" + orgId + ", name=" + name + ", type="
				+ type + ", provider=" + provider 
				+ ", username=" + username + ", password=" + password
				+ ", token=" + token + ", key=" + key + ", secret=" + secret
				+ "]";
	}
	
	
	
}
