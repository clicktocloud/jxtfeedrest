package com.ctc.jobboard.jxt.component;

import java.util.Arrays;
import java.util.List;

import com.ctc.jobboard.core.JobBoard;
import com.ctc.jobboard.exception.BadRequestException;
import com.ctc.jobboard.exception.JxtAccountException;
import com.ctc.jobboard.jxt.domain.APIReturn;
import com.ctc.jobboard.jxt.domain.ArrayOfJob;
import com.ctc.jobboard.jxt.domain.BaseRequest;
import com.ctc.jobboard.jxt.domain.Job;
import com.ctc.jobboard.jxt.domain.JobPostSummary;
import com.ctc.jobboard.jxt.domain.ext.JobPostResponse;

public abstract class JXTJobBoard extends JobBoard{
	
	protected JxtAccount defaultJxtAccount;

	protected JXTServiceDispatcher jxtServiceDispatcher;
	
	public JXTJobBoard(String jobBoardName, String jobBoardSfApiName) {
		super(jobBoardName, jobBoardSfApiName);
		
	}
	
	public JxtAccount buildJxtAccount(String advertiserId, String username, String password){
		return new JxtAccount(username, password, advertiserId);
	}
	
	protected JxtAccount getJxtAccount() throws JxtAccountException{
		if(defaultJxtAccount != null){
			return defaultJxtAccount;
		}else{
			return JxtAccountFactory.getJxtAccount(getCurrentConnection(), getCurrentSforg().getOrgId(), getJobBoardName(), getNamespace());
		}
		
	}
	
	protected JobPostResponse createJobPostResponse(APIReturn code, String message, String action, String actionMessage, List<String> referenceNos){
		if( referenceNos == null || referenceNos.size() == 0){
			return null;
		}
		
		JobPostResponse response = new JobPostResponse();
		response.setSummary(new JobPostSummary());
		response.getSummary().setFailed(1);
		response.setResponseCode(code);
		response.setResponseMessage(message);
		response.setJobPosting( new ArrayOfJob());
		
		for(String no : referenceNos){
			Job job = new Job();
			job.setAction(action);
			job.setMessage(actionMessage);
			job.setReferenceNo( no );
			response.getJobPosting().getJob().add(job);
		}

		return response;
	}
	

	protected JobPostResponse createJobPostResponse(APIReturn code, String message, String action, String actionMessage, String referenceNo){
		if( referenceNo == null ){
			return null;
		}
		
		return createJobPostResponse(code, message, action, actionMessage, Arrays.asList(new String[]{referenceNo}));
		
		
	}
	
	
	
	
	protected <R> R execute(BaseRequest request, Class<R> clazz) throws BadRequestException{
		if(jxtServiceDispatcher == null){
			jxtServiceDispatcher = new JXTServiceDispatcher();
		}
		
		
		return jxtServiceDispatcher.execute(request, clazz);
		
	}
	
	public JXTServiceDispatcher getJxtServiceDispatcher() {
		return jxtServiceDispatcher;
	}


	public void setJxtServiceDispatcher(JXTServiceDispatcher jxtServiceDispatcher) {
		this.jxtServiceDispatcher = jxtServiceDispatcher;
	}


	public JxtAccount getDefaultJxtAccount() {
		return defaultJxtAccount;
	}


	public void setDefaultJxtAccount(JxtAccount defaultJxtAccount) {
		this.defaultJxtAccount = defaultJxtAccount;
	}


}
