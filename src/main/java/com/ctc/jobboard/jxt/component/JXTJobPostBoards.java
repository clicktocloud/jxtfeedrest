package com.ctc.jobboard.jxt.component;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.ctc.jobboard.exception.BadRequestException;
import com.ctc.jobboard.exception.JxtAccountException;
import com.ctc.jobboard.exception.JxtException;
import com.ctc.jobboard.jxt.domain.APIReturn;
import com.ctc.jobboard.jxt.domain.ArrayOfJobListing;
import com.ctc.jobboard.jxt.domain.Job;
import com.ctc.jobboard.jxt.domain.JobPostRequest;
import com.ctc.jobboard.jxt.domain.ext.JobPostResponse;
import com.ctc.jobboard.util.Config;
import com.sforce.ws.ConnectionException;

public abstract class JXTJobPostBoards extends JXTJobBoard{
	
	static Logger logger = Logger.getLogger("com.ctc.jobboard");
	
	public static String getAdId(String referenceNo){
		String aid = "";
		if( referenceNo!= null && referenceNo.contains(":")){
			aid = referenceNo.split(":")[1];
			
		}
		
		return aid;
	}
	
	protected JobPostResponse response;  
	protected ArrayOfJobListing jobListings = new ArrayOfJobListing();
	
	
	
	/**
	 * Constructor 
	 * 
	 * @param jobBoardName
	 * @param jobBoardSfApiName
	 */
	protected JXTJobPostBoards(String jobBoardName, String jobBoardSfApiName) {
		super(jobBoardName, jobBoardSfApiName);
	}
	
	
	@Override
	public void post2JobBoard() {
		
		JobPostRequest request = new JobPostRequest();
		
		
		try {
			JxtAccount jxtAccount = getJxtAccount();
			if(jxtAccount == null){
				throw new JxtAccountException("Can not find available JXT Account !");
			}
			
			logger.debug("AdvertiserId = "+jxtAccount.getAdvertiserId());
			logger.debug("username = "+jxtAccount.getUsername());
			logger.debug("password = "+jxtAccount.getPassword());
			
			setJobBoarId( jxtAccount.getAdvertiserId() );
			
			//set credentials
			request.setAdvertiserId( jxtAccount.getIntAdvertiserId());
			request.setUserName(jxtAccount.getUsername());
			request.setPassword(jxtAccount.getPassword());
			request.setArchiveMissingJobs( Config.archiveMissingJobs );
			
			//
			ArrayOfJobListing listings = getJobListings();
			if( listings != null && listings.getJobListing().size() > 0){
				request.setListings(listings);
				

				JobPostResponse response = execute(request, JobPostResponse.class);
				
				setResponse( response );
				
			}else{
				logger.info("No any job to be posted from org org ["+getCurrentSforg().getOrgId()+"]");
			}
		} catch (JxtAccountException e) {
			
			throw new JxtException("JxtAccountException - " + e.getMessage());
		} catch (BadRequestException e) {
		
			throw new JxtException("BadRequestException - " + e.getMessage());
		} catch( Exception e){
			
			throw new JxtException("Other error - " + e.getMessage());
		}
	}
	
	@Override
	public void responseHandler(){
		
		if( getResponse() != null ){
			
			logger.debug("Code : " +getResponse().getResponseCode());
			logger.debug("Message : " +getResponse().getResponseMessage());
			if(getResponse().getSummary() != null){
				logger.debug("Sent : " +getResponse().getSummary().getSent());
				logger.debug("inserted : " +getResponse().getSummary().getInserted());
				logger.debug("updated : " +getResponse().getSummary().getUpdated());
				logger.debug("archived : " +getResponse().getSummary().getArchived());
				logger.debug("Failed : " +getResponse().getSummary().getFailed());
			}
			
			
		}
		try {
//			
			successHandler( );
			archivedHanler( );
			failureHandler( );
			
			updateSFJobAds( );
			
		}  catch (ConnectionException e1) {
			
			throw new JxtException("ConnectionException: orgId="+getCurrentSforg().getOrgId() +e1);
		}catch(Exception e){
			
			
			throw new JxtException("Exception: orgId="+getCurrentSforg().getOrgId() +" - "+ e);
		}
		
	}
	
	protected void archivedHanler( )  {
		
		if(getResponse() == null)
			return;
		
		List<Job> jobs = getResponse().getArchives();
		if(jobs == null  || jobs.size() == 0)
			return;
		
		for(Job job : jobs){
			
			String jobRefCode = job.getReferenceNo();
			String aid = getAdId(jobRefCode);
			if( ! StringUtils.isEmpty( aid )){
				
				addArchiveSObjects(aid);
			}
		}
	}
	
	protected void failureHandler( ){
		
		if(getResponse() == null)
			return;
		
		List<Job> jobs = getResponse().getErrors();
		if(jobs == null  || jobs.size() == 0)
			return;
		
		for(Job job : jobs){
			
			String jobRefCode = job.getReferenceNo();
			logger.debug("Failed to post job ["+job.getReferenceNo()+"] - " + job.getMessage());
			String aid = getAdId(jobRefCode);
			if( ! StringUtils.isEmpty( aid )){
				
				addFailureSObjects(aid, job.getMessage());
			}
		}
	}
	
	protected void successHandler( ) {
		if(getResponse() == null)
			return;
		
		List<Job> insertUpdateJobs = getResponse().getInsertUpdates();
		
		if(insertUpdateJobs == null  || insertUpdateJobs.size() == 0)
			return;
		for(Job job : insertUpdateJobs){
			
			String jobRefCode = job.getReferenceNo();
		
			String onlineAdUrl = job.getURL();
			
			logger.debug("onlineAdUrl : " + onlineAdUrl);
			
			String onlineJobId = "";
			if(onlineAdUrl != null && onlineAdUrl.indexOf("/") > 0 && onlineAdUrl.indexOf("/") < onlineAdUrl.length() - 1){
				onlineJobId = onlineAdUrl.substring(onlineAdUrl.lastIndexOf("/") + 1);
			}
			
			String aid = getAdId(jobRefCode);
			if( ! StringUtils.isEmpty( aid )){
				
				addSuccessSObjects(aid, getJobBoarId(), onlineAdUrl,onlineJobId,job.getAction());
			}
		}
	}
	
	protected void throwJxtExceptionFromUnsuccessfulJobPostResponse(){
		if(getResponse() != null ){
			if( getResponse().getResponseCode() != APIReturn.SUCCESS){
				throw new JxtException("JXT Response - " + getResponse().getResponseCode()+" :" + getResponse().getResponseMessage());
			}
			
		}	
	}
	
	public JobPostResponse getResponse() {
		return response;
	}

	public void setResponse(JobPostResponse response) {
		this.response = response;
	}
	
	public ArrayOfJobListing getJobListings() {
		return jobListings;
	}


	

}
