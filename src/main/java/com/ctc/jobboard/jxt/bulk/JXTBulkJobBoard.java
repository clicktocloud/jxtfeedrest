package com.ctc.jobboard.jxt.bulk;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBException;

import org.apache.log4j.Logger;

import com.ctc.jobboard.adapter.SalesforceAdapter;
import com.ctc.jobboard.jxt.component.JXTJobPostBoards;
import com.ctc.jobboard.jxt.domain.JobListing;
import com.ctc.jobboard.marshaller.MarshallerHelper;
import com.ctc.jobboard.util.SfOrg;
import com.sforce.soap.partner.QueryResult;
import com.sforce.soap.partner.sobject.SObject;
import com.sforce.ws.ConnectionException;

public class JXTBulkJobBoard extends JXTJobPostBoards {
	
	static Logger logger = Logger.getLogger("com.ctc.jobboard");
	
	
	public JXTBulkJobBoard(String jobBoardName, String jobBoardSfApiName) {
		super(jobBoardName, jobBoardSfApiName);
		
	}
	
	public List<SfOrg> loadOrgList() throws ConnectionException{
		 return SalesforceAdapter.getOrgList( getJobBoardSfApiName());
	}
	
	public  void makeJobFeeds() {
		
		List<SfOrg> orgList = null;
		try {
			orgList = loadOrgList();
		} catch (ConnectionException e1) {
			logger.error("Failed to connect to corp instance, Exit ! ", e1);
			
			return;
		}
		
		if(orgList == null || orgList.size() == 0){
			
			logger.warn("Can not find any org to post jobs to JXT, Exit !");
			
			return;
			
		}
		
		for(SfOrg sforg:orgList){
			currentSforg = sforg;
			try {
				
				currentConnection = SalesforceAdapter.getConnectionByOAuth( sforg.getOrgId() );
				
				logger.debug("Begin to make job listings for org ["+sforg.getOrgId()+"]");
				
				makeJobListings4SingleOrg();
				
				logger.debug("-----------------------------------------");
				
				//
				logger.debug("Begin to post jobs to job board["+jobBoardName+"]");
				
				post2JobBoard();
				
				logger.debug("-----------------------------------------");
				
				responseHandler();
			} catch (Exception e) {
				logger.error("Exception : orgId="+sforg.getOrgId()+"  "+e);
			}
		}
	}

	
	
	public void makeJobListings4SingleOrg() throws ConnectionException{
		
		jobListings.getJobListing().clear();
		
		
		Map<String, String> jobContents =  retrieveJobContents4SingleOrg();
		for(Map.Entry<String, String> job : jobContents.entrySet()){
			
			String jobContent = job.getValue();
			if(jobContent!=null){
				
				try {
					JobListing jobListing = MarshallerHelper.convertJsontoObject(jobContent, JobListing.class);
					jobListings.getJobListing().add(jobListing);
				} catch (JAXBException e) {
					e.printStackTrace();
					logger.error("Invalid job content of job ["+job.getKey()+"], ignore !");
					//logger.debug(e);
				}		
			}	
		}
	}
	
	private Map<String, String> retrieveJobContents4SingleOrg() throws ConnectionException{
		
		Map<String, String> jobContents = new HashMap<String, String>();
		
		QueryResult qr = currentConnection.query( SalesforceAdapter.getJobContentQueryString(jobBoardName, getNamespace()));
		
		boolean done = false;
		logger.debug("qr size = "+qr.getSize());
		if(qr.getSize()>0){
			while(!done){
				for(SObject sobj : qr.getRecords()){
					String id = sobj.getId();
				
					if(id != null ){
						if(sobj.getField(getNamespace() + "Placement_Date__c")==null){
							adStatusMap.put(id, "");
						}else{
							adStatusMap.put(id, sobj.getField(getNamespace() + "Placement_Date__c").toString());
						}
						
						if(sobj.getField(getNamespace() + "Job_Posting_Status__c")==null){
							adPostingStatuses.put(id, "");
						}else{
							adPostingStatuses.put(id, sobj.getField(getNamespace() + "Job_Posting_Status__c").toString());
						}
						
						String jobContent = (String) sobj.getField(getNamespace() + "JobXML__c");
						if(jobContent!=null){
							
							jobContents.put(id, jobContent);
							
						}
					}	
				}
				if(qr.getDone()){
					done = true;
				}else{
					qr = currentConnection.queryMore(qr.getQueryLocator());
				}
			}
		}
		
		return jobContents;
	}	
	
	
	

}
