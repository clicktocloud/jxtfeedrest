package com.ctc.jobboard.jxt.bulk;

import org.apache.log4j.Logger;

import com.ctc.jobboard.util.Config;


public class JxtnzClient extends JXTBulkJobBoard{

	static Logger logger = Logger.getLogger("com.ctc.jobboard");
	
	public JxtnzClient(String jobBoardName, String jobBoardSfApiName) {
		super(jobBoardName, jobBoardSfApiName);
	}

	
	public static void start() throws Exception{
		JxtnzClient jxtnz = new JxtnzClient("JXT_NZ","JXT_NZ__c");
		
		Config.archiveMissingJobs = true;
		
		try {
			
			jxtnz.makeJobFeeds();
			
		}  catch (Exception e) {
			logger.error("JXT_NZ Client start error - " , e);
		}
	}
}
