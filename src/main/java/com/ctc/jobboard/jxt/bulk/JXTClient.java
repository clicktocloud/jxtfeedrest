package com.ctc.jobboard.jxt.bulk;

import org.apache.log4j.Logger;

public class JXTClient extends JXTBulkJobBoard{
	static Logger logger = Logger.getLogger("com.ctc.jobboard");
	public JXTClient(String jobBoardName, String jobBoardSfApiName) {
		super(jobBoardName, jobBoardSfApiName);
	}

	public static void start() throws Exception{
		JXTClient jxt = new JXTClient("JXT","JXT__c");
		try {
			
			jxt.makeJobFeeds();
			
		}  catch (Exception e) {
			logger.error("JXTClient start error - " , e);
		}
	}
}
