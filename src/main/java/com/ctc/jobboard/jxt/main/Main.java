package com.ctc.jobboard.jxt.main;

import org.apache.log4j.Logger;

import com.ctc.jobboard.jxt.bulk.JXTClient;
import com.ctc.jobboard.jxt.bulk.JxtnzClient;


public class Main {
	static Logger logger = Logger.getLogger("com.ctc.jobboard");
	public static void main(String[] args) throws Exception{
		String boardName = "jxt";
		if(args.length > 0)
			boardName = args[0];
		
		//JxtnzClient.start();
		
		if("jxt".equalsIgnoreCase( boardName )){
			logger.debug("jxtFeed.jar jxt");
			JXTClient.start();
		}else if("jxtnz".equalsIgnoreCase( boardName )){
			logger.debug("jxtFeed.jar jxtnz");
			JxtnzClient.start();
		}
	}
}
