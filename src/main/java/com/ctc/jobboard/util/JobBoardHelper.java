package com.ctc.jobboard.util;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;



public class JobBoardHelper {
	
	public static String readFileToString(String filename) throws IOException{
		
		
		InputStream stream = JobBoardHelper.class.getResourceAsStream(filename);
		
		String ret = IOUtils.toString(stream,"utf-8");
		
		stream.close();
		return ret;
	}


	public static String removeBadChars(String s) {
	  if (s == null) return null;
	  StringBuilder sb = new StringBuilder();
	  for(int i=0;i<s.length();i++){ 
	    if (Character.isHighSurrogate(s.charAt(i))) continue;
	    sb.append(s.charAt(i));
	  }
	  return sb.toString();
	}
	
	public static String removeBadUTF8Chars(String src){
		if(src != null){
			
		
			return src.replaceAll("[^\\p{ASCII}]", "");

		}
		
		return null;
	}

}
