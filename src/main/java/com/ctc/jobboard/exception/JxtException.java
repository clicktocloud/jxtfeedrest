package com.ctc.jobboard.exception;

public class JxtException extends RuntimeException {
	private static final long serialVersionUID = -6067294234822229081L;

	public JxtException() {
		super();
	}

	public JxtException(String message, Throwable cause) {
		super(message, cause);
	}

	public JxtException(String message) {
		super(message);
	}

	public JxtException(Throwable cause) {
		super(cause);
	}

}
