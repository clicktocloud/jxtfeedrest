package com.ctc.jobboard.exception;

public class JxtAccountException extends JxtException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public JxtAccountException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public JxtAccountException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public JxtAccountException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public JxtAccountException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
	

}
