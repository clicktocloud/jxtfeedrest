package com.ctc.jobboard.exception;

public class BadRequestException extends Exception {

	/**
	 * Serialization Id
	 */
	private static final long serialVersionUID = 1L;
	
	private final int statusCode;
	
	private String additionalInformation;
	
	private String ref;

	public BadRequestException(int statusCode, String message) {
		super(message);
		this.statusCode = statusCode;
	}

	public int getStatusCode() {
		return this.statusCode;
	}
	
	public String getAdditionalInformation() {
		return additionalInformation;
	}

	public void setAdditionalInformation(String additionalInformation) {
		this.additionalInformation = additionalInformation;
	}

	public String getRef() {
		return ref;
	}

	public void setRef(String ref) {
		this.ref = ref;
	}
	
}
