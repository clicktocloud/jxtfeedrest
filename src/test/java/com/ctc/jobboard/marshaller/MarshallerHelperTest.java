package com.ctc.jobboard.marshaller;

import javax.xml.bind.JAXBException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ctc.jobboard.jxt.domain.DefaultResponse;



public class MarshallerHelperTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testConvertXMLtoObject() {
		String xml = "<DefaultResponse xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns=\"http://schemas.servicestack.net/types\"><ResponseCode xmlns=\"http://schemas.datacontract.org/2004/07/JXTPlatform.DTO.Base\">LOGIN_FAILED</ResponseCode><ResponseMessage xmlns=\"http://schemas.datacontract.org/2004/07/JXTPlatform.DTO.Base\">Advertiser Id, Username and Password should be provided</ResponseMessage><DefaultList i:nil=\"true\" /></DefaultResponse>";
	
		try {
			DefaultResponse resp = MarshallerHelper.convertXMLtoObject(xml, DefaultResponse.class);
			
			System.out.println(resp.getResponseCode());
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
