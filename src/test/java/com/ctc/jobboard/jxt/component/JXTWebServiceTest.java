package com.ctc.jobboard.jxt.component;

import java.io.IOException;
import java.util.List;

import javax.xml.bind.JAXBException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.ctc.jobboard.exception.BadRequestException;
import com.ctc.jobboard.jxt.domain.AdvertiserJobTemplateLogo;
import com.ctc.jobboard.jxt.domain.ArchiveJobRequest;
import com.ctc.jobboard.jxt.domain.ArrayOfJobListing;
import com.ctc.jobboard.jxt.domain.ConsultantList;
import com.ctc.jobboard.jxt.domain.ConsultantRequest;
import com.ctc.jobboard.jxt.domain.ConsultantResponse;
import com.ctc.jobboard.jxt.domain.CountryLocationArea;
import com.ctc.jobboard.jxt.domain.DefaultRequest;
import com.ctc.jobboard.jxt.domain.DefaultResponse;
import com.ctc.jobboard.jxt.domain.JobListing;
import com.ctc.jobboard.jxt.domain.JobPostRequest;
import com.ctc.jobboard.jxt.domain.JobTemplate;
import com.ctc.jobboard.jxt.domain.ProfessionRole;
import com.ctc.jobboard.jxt.domain.WorkType;
import com.ctc.jobboard.jxt.domain.ext.ArrayOfJob;
import com.ctc.jobboard.jxt.domain.ext.Job;
import com.ctc.jobboard.jxt.domain.ext.JobPostResponse;
import com.ctc.jobboard.marshaller.MarshallerHelper;
import com.ctc.jobboard.util.JobBoardHelper;


public class JXTWebServiceTest {
	
	static JXTServiceDispatcher service = new JXTServiceDispatcher();

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Ignore
	@Test
	public void testDefaultRequest() throws BadRequestException {
		
		DefaultRequest request = new DefaultRequest();
		request.setAdvertiserId(10546);
		
		request.setUserName("user");
		request.setPassword("c2cTest1");
		DefaultResponse response = service.execute(request, DefaultResponse.class);
		
		System.out.println(response.getResponseCode());
		System.out.println(response.getResponseMessage());
		
		System.out.println("JobTemplate : -----------------------");
		List<JobTemplate> jts = response.getDefaultList().getJobTemplates().getJobTemplate();
		for(JobTemplate jt : jts){
			System.out.println(jt.getJobTemplateID() + "\t" + jt.getJobTemplateDescription() );
			
		}
		
		System.out.println("AdvertiserJobTemplateLogo : -----------------------");
		List<AdvertiserJobTemplateLogo> jtls = response.getDefaultList().getAdvertiserJobTemplateLogos().getAdvertiserJobTemplateLogo();
		for(AdvertiserJobTemplateLogo jtl : jtls){
			System.out.println(jtl.getAdvertiserJobTemplateLogoID()+ "\t" + jtl.getLogoName() );
			
		}
		
		System.out.println("ProfessionRole : -----------------------");
		List<ProfessionRole> prs = response.getDefaultList().getProfessionRoles().getProfessionRole();
		for(ProfessionRole pr : prs){
			System.out.println(pr.getProfessionID() + "\t"+pr.getProfessionName() + "\t" + pr.getRoleID() +"\t"+pr.getRoleName());
		}
		
		System.out.println("WorkType : -----------------------");
		List<WorkType> wts = response.getDefaultList().getWorkTypes().getWorkType();
		for( WorkType wt : wts){
			System.out.println(wt.getWorkTypeID() +"\t"+wt.getWorkTypeName());
		}
		
		System.out.println("CountryLocationArea : -----------------------");
		List<CountryLocationArea> clas = response.getDefaultList().getCountryLocationAreas().getCountryLocationArea();
		for( CountryLocationArea cla : clas){
			System.out.println(cla.getLocationID() +"\t"+cla.getLocationName()+"\t"+cla.getAreaID()+"\t"+cla.getAreaName()+"\t"+cla.getCountryID()+"\t"+cla.getCountryName());
		}
	}
	

	@Test
	public void testConsultantRequest() throws BadRequestException {
		
		ConsultantRequest request = new ConsultantRequest();
		/*
		request.setAdvertiserId(10546);
		request.setUserName("user");
		request.setPassword("c2cTest1");
		
		*/
		
		request.setAdvertiserId(16062);
		request.setUserName("cherry@clicktocloud.com");
		request.setPassword("JXTc2c@Y");
		
		ConsultantResponse response = service.execute(request, ConsultantResponse.class);
		
		System.out.println(response.getResponseCode());
		System.out.println(response.getResponseMessage());
		
		for(ConsultantList c : response.getConsultants().getConsultantList()){
			
			System.out.println(c.getConsultantID());
			
		}
		
	}
	@Ignore
	@Test
	public void testJobPostRequest() throws BadRequestException, IOException, JAXBException{
		String xml = JobBoardHelper.readFileToString("/JobPostRequestSample.xml");
		JobPostRequest request = MarshallerHelper.convertXMLtoObject(xml, JobPostRequest.class);
		
		request.setAdvertiserId(10546);
		request.setUserName("user");
		request.setPassword("c2cTest1");
		
		JobPostResponse response = service.execute(request, JobPostResponse.class);
		System.out.println(response.getResponseCode());
		System.out.println(response.getResponseMessage());
		
		for( com.ctc.jobboard.jxt.domain.Job job : response.getJobPosting().getJob()){
			System.out.println(job.getAction() +"\t" + job.getReferenceNo()+"\t"+job.getMessage());
		}
	}
	
	
	@Ignore
	@Test
	public void testJobPostRequest_SetJobListing() throws BadRequestException, IOException, JAXBException{
		
		JobPostRequest request = new JobPostRequest();
		
		request.setAdvertiserId(10546);
		request.setUserName("user");
		request.setPassword("c2cTest1");
		request.setArchiveMissingJobs(false);
		
		
		String jobListingXml = JobBoardHelper.readFileToString("/JobListingSample.xml");
		JobListing jobListing = MarshallerHelper.convertXMLtoObject(jobListingXml, JobListing.class);
		ArrayOfJobListing listings = new ArrayOfJobListing();
		listings.getJobListing().add(jobListing);
		
		request.setListings(listings);
		
		
		JobPostResponse response = service.execute(request, JobPostResponse.class);
		
		System.out.println(response.getResponseCode());
		System.out.println(response.getResponseMessage());
		
		for( com.ctc.jobboard.jxt.domain.Job job : response.getJobPosting().getJob()){
			System.out.println(job.getAction() +"\t" + job.getReferenceNo()+"\t"+job.getMessage());
		}
	}
	
	
	@Ignore
	@Test
	public void testArchiveJobRequest() throws BadRequestException {
		
		ArchiveJobRequest request = new ArchiveJobRequest();
		request.setAdvertiserId(10546);
		request.setUserName("user");
		request.setPassword("c2cTest1");
		
		ArrayOfJob jobs = new ArrayOfJob();
		Job job = new Job();
		job.setReferenceNo("TESTJOB-1");
		jobs.getJob().add(job);
		
		request.setListings(jobs);
		
		
		
		JobPostResponse response = service.execute(request, JobPostResponse.class);
		
		System.out.println(response.getResponseCode());
		System.out.println(response.getResponseMessage());
		
	}

}
