package com.ctc.jobboard.jxt.response;



import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.List;

import javax.xml.bind.JAXBException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ctc.jobboard.jxt.domain.Job;
import com.ctc.jobboard.jxt.domain.ext.JobPostResponse;
import com.ctc.jobboard.marshaller.MarshallerHelper;
import com.ctc.jobboard.util.JobBoardHelper;

public class JobPostResponseTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() throws IOException, JAXBException {
		String xml = JobBoardHelper.readFileToString("/JobPostResponseSample.xml");
		JobPostResponse r = MarshallerHelper.convertXMLtoObject(xml, JobPostResponse.class);
		
		//System.out.println(r.getSummary().getDateCreated());
		assertEquals(Integer.valueOf(3), r.getSummary().getSent());
		
		List<Job> errors = r.getErrors();
		assertEquals(1, errors.size());
		assertEquals("TESTJOB-1", errors.get(0).getReferenceNo());
		assertEquals(" -> Salary Max needs to be greater than Salary Min - 200000.00 / 20000.00",errors.get(0).getMessage());
		List<Job> updates = r.getUpdates();
		assertEquals(2, updates.size());
		assertEquals("TESTJOB-3", updates.get(1).getReferenceNo());
		
	}

}
