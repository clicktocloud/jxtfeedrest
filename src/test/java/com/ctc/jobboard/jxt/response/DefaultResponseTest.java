package com.ctc.jobboard.jxt.response;

import static org.junit.Assert.assertEquals;

import javax.xml.bind.JAXBException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ctc.jobboard.jxt.domain.APIReturn;
import com.ctc.jobboard.jxt.domain.AdvertiserJobTemplateLogo;
import com.ctc.jobboard.jxt.domain.ArrayOfAdvertiserJobTemplateLogo;
import com.ctc.jobboard.jxt.domain.ArrayOfCountryLocationArea;
import com.ctc.jobboard.jxt.domain.ArrayOfJobTemplate;
import com.ctc.jobboard.jxt.domain.ArrayOfProfessionRole;
import com.ctc.jobboard.jxt.domain.ArrayOfWorkType;
import com.ctc.jobboard.jxt.domain.CountryLocationArea;
import com.ctc.jobboard.jxt.domain.DefaultList;
import com.ctc.jobboard.jxt.domain.DefaultResponse;
import com.ctc.jobboard.jxt.domain.JobTemplate;
import com.ctc.jobboard.jxt.domain.ProfessionRole;
import com.ctc.jobboard.jxt.domain.WorkType;
import com.ctc.jobboard.marshaller.MarshallerHelper;


public class DefaultResponseTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() throws JAXBException {
		DefaultList dl = new DefaultList();
		
		dl.setJobTemplates(new ArrayOfJobTemplate());
		dl.setWorkTypes(new ArrayOfWorkType());
		dl.setCountryLocationAreas(new ArrayOfCountryLocationArea());
		dl.setProfessionRoles(new ArrayOfProfessionRole());
		dl.setAdvertiserJobTemplateLogos(new ArrayOfAdvertiserJobTemplateLogo());
		
		//JobTemplates
		JobTemplate jt = new JobTemplate();
		jt.setJobTemplateID("111");
		jt.setJobTemplateDescription("Test job template 111");
		dl.getJobTemplates().getJobTemplate().add(jt);
		
		jt = new JobTemplate();
		jt.setJobTemplateID("222");
		jt.setJobTemplateDescription("Test job template 222");
		dl.getJobTemplates().getJobTemplate().add(jt);
		
		//CountryLocationAreas
		CountryLocationArea ca = new CountryLocationArea();
		ca.setCountryID("1");
		ca.setCountryName("AUSTRALIA");
		ca.setLocationID("1");
		ca.setLocationName("Sydney");
		ca.setAreaID("1");
		ca.setAreaName("All Sydney");
		dl.getCountryLocationAreas().getCountryLocationArea().add(ca);
		
		//ProfessionRoles
		ProfessionRole pr = new ProfessionRole();
		pr.setProfessionID("3");
		pr.setProfessionName("Accounting");
		pr.setRoleID("1");
		pr.setRoleName("Accountant - Assistant/Graduate");
		dl.getProfessionRoles().getProfessionRole().add(pr);
		
		//AdvertiserJobTemplateLogos
		AdvertiserJobTemplateLogo al = new AdvertiserJobTemplateLogo();
		al.setAdvertiserJobTemplateLogoID("111");
		al.setLogoName("Test Template 1");
		dl.getAdvertiserJobTemplateLogos().getAdvertiserJobTemplateLogo().add(al);
		
		//WorkTypes
		WorkType wt = new WorkType();
		wt.setWorkTypeID("1");
		wt.setWorkTypeName("Part Time");
		dl.getWorkTypes().getWorkType().add(wt);
		
		DefaultResponse r = new DefaultResponse();
		r.setResponseCode(APIReturn.SUCCESS);
		r.setResponseMessage("Test Success");
		r.setDefaultList(dl);
		
		String xml = MarshallerHelper.convertObjecttoXML(r);
		
		System.out.println(xml);
		
		DefaultResponse r2 = MarshallerHelper.convertXMLtoObject(xml, DefaultResponse.class);
		assertEquals(r.getResponseCode(), r2.getResponseCode());
		
		assertEquals(r.getDefaultList().getJobTemplates().getJobTemplate().get(0).getJobTemplateID(), r2.getDefaultList().getJobTemplates().getJobTemplate().get(0).getJobTemplateID());
		assertEquals(r.getDefaultList().getWorkTypes().getWorkType().get(0).getWorkTypeName(), r2.getDefaultList().getWorkTypes().getWorkType().get(0).getWorkTypeName());
		assertEquals(r.getDefaultList().getCountryLocationAreas().getCountryLocationArea().get(0).getCountryID(),r2.getDefaultList().getCountryLocationAreas().getCountryLocationArea().get(0).getCountryID());
		assertEquals(r.getDefaultList().getProfessionRoles().getProfessionRole().get(0).getProfessionID(),r2.getDefaultList().getProfessionRoles().getProfessionRole().get(0).getProfessionID());
		assertEquals(r.getDefaultList().getAdvertiserJobTemplateLogos().getAdvertiserJobTemplateLogo().get(0).getLogoName(),r2.getDefaultList().getAdvertiserJobTemplateLogos().getAdvertiserJobTemplateLogo().get(0).getLogoName());
	}

}
