package com.ctc.jobboard.jxt.service;

import java.io.IOException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.ctc.jobboard.core.JobBoard;
import com.ctc.jobboard.jxt.component.JXTServiceDispatcher;
import com.ctc.jobboard.jxt.domain.ext.JobPostResponse;
import com.ctc.jobboard.util.Config;
import com.ctc.jobboard.util.JobBoardHelper;

public class JXTPostJobServiceTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Ignore
	@Test
	public void test() throws IOException {
		JXTPostJobService postService = new JXTPostJobService("JXT","JXT__c");
		
		String jobXmlContent = JobBoardHelper.readFileToString("/JobListingSample2.xml");
		postService.setJxtServiceDispatcher(new JXTServiceDispatcher());
		JobPostResponse response = postService.postJob("00D90000000gtFz", "00D90000000gtFz:a0090000005cDR5AAM",JobBoard.SF_POSTING_STATUS_INQUEUE, jobXmlContent);
		
		System.out.println("Posted : " + (response.getSummary().getInserted() + response.getSummary().getUpdated()));
		
	}
	@Ignore
	@Test
	public void testJsonJob() throws IOException {
		JXTPostJobService postService = new JXTPostJobService("JXT","JXT__c");
		
		String jobJsonContent = JobBoardHelper.readFileToString("/JobListingSample.json");
		postService.setJxtServiceDispatcher(new JXTServiceDispatcher());
		//JobPostResponse response = postService.postJsonJob("00D90000000gtFz", "00D90000000gtFz:a0090000005cDR5AAM", jobJsonContent);
		JobPostResponse response = postService.postJsonJob("00D90000000gtFz", "00D90000000gtFz:a0090000005cDR5AAM",JobBoard.SF_POSTING_STATUS_INQUEUE, jobJsonContent,Config.TEST_ADVERTISER_ID,Config.TEST_USERNAME,Config.TEST_PASSWORD,"");
		
	
		System.out.println("Posted : " + (response.getSummary().getInserted() + response.getSummary().getUpdated()));
		
	}
	
	@Test
	public void testYUDUJsonJob() throws IOException {
		JXTPostJobService postService = new JXTPostJobService("JXT","JXT__c");
		
		String jobJsonContent = JobBoardHelper.readFileToString("/YUDUJobListingSample.json");
		postService.setJxtServiceDispatcher(new JXTServiceDispatcher());
		
		JobPostResponse response = postService.postJsonJob(
				"00D90000000gtFz", 
				"00D90000000gtFz:a009000003eeoyAAAQ",
				JobBoard.SF_POSTING_STATUS_INQUEUE, 
				jobJsonContent,
				"16062",
				"cherry@clicktocloud.com",
				"JXTc2c@Y",
				"PeopleCloudS__");
		
		System.out.println("Posted : " + (response.getSummary().getInserted() + response.getSummary().getUpdated()));
		
		
	}
	

}
