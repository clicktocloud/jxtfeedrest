package com.ctc.jobboard.jxt.service;

import java.io.IOException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.ctc.jobboard.jxt.component.JXTServiceDispatcher;
import com.ctc.jobboard.jxt.domain.ext.Area;
import com.ctc.jobboard.jxt.domain.ext.Classification;
import com.ctc.jobboard.jxt.domain.ext.Country;
import com.ctc.jobboard.jxt.domain.ext.Location;
import com.ctc.jobboard.jxt.domain.ext.SubClassification;
import com.ctc.jobboard.util.Config;

public class JXTGetDefaultListServiceTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	
	@Test
	public void testYUDU() throws IOException {
		JXTGetDefaultListService defaultService = new JXTGetDefaultListService("JXT","JXT__c");
		
		defaultService.setJxtServiceDispatcher(new JXTServiceDispatcher());
		
		
		defaultService.retrieveDefaultList(Config.YUDU_TEST_ADVERTISER_ID, Config.YUDU_TEST_USERNAME, Config.YUDU_TEST_PASSWORD);
		
	
		System.out.println("Countries");
		for(Country c : defaultService.getCountryList().getCountries()){
			
			System.out.println(c.getCountryId() + " - " + c.getCountryName());
			
		}
		
		System.out.println("Locations");
		for(Location l : defaultService.getLocationList().getLocation()){
			
			System.out.println(l.getLocationId() + " - " + l.getLocationName() + " - " + l.getCountryId());
			
		}
		
		
		System.out.println("Areas");
		for(Area a : defaultService.getAreaList().getArea()){
			
			System.out.println(a.getAreaId() + " - " + a.getAreaName() + " - " + a.getLocationId() + " - " + a.getCountryId());
			
		}
		
		System.out.println("Classification");
		for(Classification c  : defaultService.getClassificationList().getClassification()){
			
			System.out.println(c.getId()+ " - " + c.getName() );
			
		}
		
		System.out.println("SubClassification");
		for(SubClassification c  : defaultService.getSubClassificationList().getSubClassification()){
			
			System.out.println(c.getId()+ " - " + c.getName() + " - " + c.getParentId() );
			
		}	
	}
	
	@Ignore
	@Test
	public void test() throws IOException {
		JXTGetDefaultListService defaultService = new JXTGetDefaultListService("JXT","JXT__c");
		
		defaultService.setJxtServiceDispatcher(new JXTServiceDispatcher());
		
		//defaultService.retrieveDefaultList("00D90000000gtFz");
		defaultService.retrieveDefaultList(Config.TEST_ADVERTISER_ID, Config.TEST_USERNAME, Config.TEST_PASSWORD);
		
	
		System.out.println("Countries");
		for(Country c : defaultService.getCountryList().getCountries()){
			
			System.out.println(c.getCountryId() + " - " + c.getCountryName());
			
		}
		
		System.out.println("Locations");
		for(Location l : defaultService.getLocationList().getLocation()){
			
			System.out.println(l.getLocationId() + " - " + l.getLocationName() + " - " + l.getCountryId());
			
		}
		
		
		System.out.println("Areas");
		for(Area a : defaultService.getAreaList().getArea()){
			
			System.out.println(a.getAreaId() + " - " + a.getAreaName() + " - " + a.getLocationId() + " - " + a.getCountryId());
			
		}
		
		System.out.println("Classification");
		for(Classification c  : defaultService.getClassificationList().getClassification()){
			
			System.out.println(c.getId()+ " - " + c.getName() );
			
		}
		
		System.out.println("SubClassification");
		for(SubClassification c  : defaultService.getSubClassificationList().getSubClassification()){
			
			System.out.println(c.getId()+ " - " + c.getName() + " - " + c.getParentId() );
			
		}	
	}


}
