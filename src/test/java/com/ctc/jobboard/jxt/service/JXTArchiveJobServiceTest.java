package com.ctc.jobboard.jxt.service;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.ctc.jobboard.jxt.component.JXTServiceDispatcher;
import com.ctc.jobboard.jxt.domain.ext.JobPostResponse;
import com.ctc.jobboard.util.Config;

public class JXTArchiveJobServiceTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Ignore
	@Test
	public void test() throws IOException {
		JXTArchiveJobService archiveService = new JXTArchiveJobService("JXT","JXT__c");
		
		archiveService.setJxtServiceDispatcher(new JXTServiceDispatcher());
		
		List<String> referenceNos = Arrays.asList(new String[]{"00D90000000gtFz:a0090000005cDR5AAM"});
		JobPostResponse response = archiveService.archiveJob("00D90000000gtFz", referenceNos);
		//JobPostResponse response = archiveService.archiveJob("00D90000000gtFz", referenceNos, Config.TEST_ADVERTISER_ID, Config.TEST_USERNAME, Config.TEST_PASSWORD, "");
		
		System.out.println("Archived : " + response.getSummary().getArchived());
		
		
	}
	
	@Test
	public void testYUDU() throws IOException {
		JXTArchiveJobService archiveService = new JXTArchiveJobService("JXT","JXT__c");
		
		archiveService.setJxtServiceDispatcher(new JXTServiceDispatcher());
		
		List<String> referenceNos = Arrays.asList(new String[]{"00D90000000gtFz:a009000003eeoyAAAQ"});
		//JobPostResponse response = archiveService.archiveJob("00D90000000gtFz", referenceNos);
		JobPostResponse response = archiveService.archiveJob("00D90000000gtFz", referenceNos, Config.YUDU_TEST_ADVERTISER_ID, Config.YUDU_TEST_USERNAME, Config.YUDU_TEST_PASSWORD, Config.namespace);
		
		System.out.println("Archived : " + response.getSummary().getArchived());
		
		
	}


}
