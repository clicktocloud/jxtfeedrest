package com.ctc.jobboard.jxt.request;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import javax.xml.bind.JAXBException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ctc.jobboard.jxt.domain.JobListing;
import com.ctc.jobboard.jxt.domain.JobPostRequest;
import com.ctc.jobboard.marshaller.MarshallerHelper;
import com.ctc.jobboard.util.JobBoardHelper;

public class JobPostRequestTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	

	@Test
	public void testXmlContent() throws JAXBException, IOException {
		String xml = JobBoardHelper.readFileToString("/JobPostRequestSample.xml");
		JobPostRequest r = MarshallerHelper.convertXMLtoObject(xml, JobPostRequest.class);
		
		assertEquals("USERNAME", r.getUserName());
		
		JobListing job = r.getListings().getJobListing().get(0);
		
		assertEquals("TESTJOB-1",job.getJobTitle());
		assertEquals("38",job.getCategories().getCategory().get(0).getClassification());
		assertEquals("3",job.getListingClassification().getWorkType());
		
	}
	


}
