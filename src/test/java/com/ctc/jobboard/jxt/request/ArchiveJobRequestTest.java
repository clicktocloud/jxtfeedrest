package com.ctc.jobboard.jxt.request;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import javax.xml.bind.JAXBException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ctc.jobboard.jxt.domain.ArchiveJobRequest;
import com.ctc.jobboard.marshaller.MarshallerHelper;
import com.ctc.jobboard.util.JobBoardHelper;

public class ArchiveJobRequestTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() throws JAXBException, IOException {
		String xml = JobBoardHelper.readFileToString("/ArchiveJobRequestSample.xml");
		ArchiveJobRequest r = MarshallerHelper.convertXMLtoObject(xml, ArchiveJobRequest.class);
		
		assertEquals("USERNAME", r.getUserName());
		assertEquals(1111, r.getAdvertiserId().intValue());
		
		assertEquals(2, r.getListings().getJob().size());
		
		assertEquals("REFNOO-1", r.getListings().getJob().get(0).getReferenceNo());
		
		
	}

}
