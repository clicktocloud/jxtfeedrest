package com.ctc.jobboard.jxt.request;

import java.io.IOException;

import javax.xml.bind.JAXBException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.ctc.jobboard.jxt.domain.JobListing;
import com.ctc.jobboard.marshaller.MarshallerHelper;
import com.ctc.jobboard.util.JobBoardHelper;

public class JobListingTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	

	@Ignore
	@Test
	public void test() throws JAXBException, IOException {
		String xml = JobBoardHelper.readFileToString("/JobListingSample2.xml");
		JobListing j = MarshallerHelper.convertXMLtoObject(xml, JobListing.class);
		
		System.out.println(j.getReferenceNo());
		
	}
	
	@Test
	public void testJson() throws JAXBException, IOException {
		String jsonString = JobBoardHelper.readFileToString("/JobListingSample.json");
		JobListing j = MarshallerHelper.convertJsontoObject(jsonString, JobListing.class);
		
		System.out.println(j.getExpiryDate() == null);
		
		String xml = MarshallerHelper.convertObjecttoXML(j);
		
		j = MarshallerHelper.convertXMLtoObject(xml, JobListing.class);
		
		System.out.println(j.getJobFullDescription());
		
	}
}
