package com.ctc.jobboard.jxt.adapter;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.apache.commons.lang.StringUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.ctc.jobboard.adapter.SalesforceAdapter;
import com.ctc.jobboard.jxt.component.JxtAccount;
import com.ctc.jobboard.util.Config;
import com.sforce.soap.partner.PartnerConnection;
import com.sforce.ws.ConnectionException;

public class SalesforceAdapterTest {
	
	static PartnerConnection conn = null;

	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		conn = SalesforceAdapter.getConnectionByOAuth("00D90000000gtFz");
	}

	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}


	@Ignore
	@Test
	public void testGetJxtAccount() throws ConnectionException {
		
		
		
		JxtAccount jxtAccount = SalesforceAdapter.getJxtAccount(conn, "JXT", "");
		
		System.out.println(jxtAccount);
		assertFalse(StringUtils.isEmpty(jxtAccount.getUsername()));
		assertFalse(StringUtils.isEmpty(jxtAccount.getPassword()));
		
	}
	
	
	@Test 
	public void testObjectHasField() throws ConnectionException{
		
		
		assertTrue(SalesforceAdapter.objectHasField(conn, "PeopleCloudS__Advertisement_Configuration__c", "PeopleCloudS__UserName__c"));
	}
	

	@Test 
	public void testGetJxtAccountFromAdConfig() throws ConnectionException{
		
		
		JxtAccount a = SalesforceAdapter.getJxtAccountFromAdConfig(conn, "JXT", Config.namespace);
		
		System.out.println(a);
		assertNotNull(a);
		
		SalesforceAdapter.setJxtAccountCredentialByRestful(conn, a, Config.namespace);
		
		System.out.println(a);
		assertNotNull(a);
	}

}
