package com.ctc.jobboard.util;

import static org.junit.Assert.assertEquals;

import java.nio.charset.CharacterCodingException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class JobBoardHelperTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testRemoveBadUTF8Chars() throws CharacterCodingException {
		
		
		String src = "abc?_\t\n\r";
		assertEquals(src,JobBoardHelper.removeBadUTF8Chars(src));
		System.out.println(src);
		String x = String.valueOf(new char[]{'â','a','b','\uFFFF','c'});
		System.out.println(x);
		System.out.println("x=" + JobBoardHelper.removeBadUTF8Chars(x));
		String[] values = {String.valueOf(new char[]{'â','A','B',0xe2,'c'}), "\\xF0\\x9F\\x91\\x8C", "/*��", "look into my eyes 〠.〠", "fkdjsf ksdjfslk�", "\\xF0\\x80\\x80\\x80", "aa \\xF0\\x9F\\x98\\x95 aa", "Ok"};
		for (int i = 0; i < values.length; i++) {
		    System.out.println(JobBoardHelper.removeBadUTF8Chars(values[i]));
		}
		
	}

}
